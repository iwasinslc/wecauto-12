<?php

use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\Wallet;

/**
 * Check if user is authorized.
 * @return bool
 *
 * http://demo.hyipium.com/admin/see_integration_example/isUserAuthorized
 */
function isUserAuthorized(): bool
{
    return (boolean)\Auth::user();
}


function getPhotoPath($path)
{

    try {
        return Storage::disk('s3')->url($path);
    } catch (\Exception $e) {
        return '/assets/images/orders/car.png';
    }
}


/**
 * @return \App\Models\User
 *
 */

function user()
{

    return \Auth::user();
}

function allBalance()
{

    $usd = getUserWallet('USD', 'perfectmoney')->balance;
    $acc = getUserWallet('ACC')->balance*rate('ACC', 'USD');
    $wec = getUserWallet('WEC')->balance*rate('WEC', 'USD');

    $orders = cache()->tags('MyOrdersBalance')->remember('MyOrdersBalance.'.user()->id,  now()->addMinutes(30), function ()  {
        return user()->exchangeOrders()->active()->where('main_currency_id', \App\Models\Currency::getByCode('USD')->id)->sum('amount');
    });

    return number_format($usd+$acc+$wec+$orders,2);
}

/**
 * Get if customer has closed deposits
 * @return mixed
 * @throws Exception
 */
function hasClosedDeposits()
{
    return cache()->rememberForever('userHasClosedDeposits.' . user()->id, function() {
        return \App\Models\Deposit::query()
            ->where('user_id', user()->id)
            ->where('active', false)
            ->count() > 0;
    });
}

/**
 * Check if customer already sent request
 * @return mixed
 * @throws Exception
 */
function isCashbackRequestSent()
{
    //return cache()->rememberForever('i.isCashbackRequestSent.' . user()->id, function() {
        // Count all approved or in progress requests
        $cashbackRequestCount = user()
            ->cashback()
            ->where(function ($query) {
                $query->where('approved', true)
                    ->orWhereNull('approved');
            })

            ->count();
        return $cashbackRequestCount > 0; // Customer has already sent request if true
    //});
}

/*
 * Non-authorized user part of helpers
 */

/**
 * How long project is active.
 *
 * @return int
 * @throws
 *
 * http://demo.hyipium.com/admin/see_integration_example/getRunningDays
 */
function getRunningDays(): int
{
    return cache()->remember('i.runningDays', now()->addHour(), function () {
        $flag = 'created_at';
        $firstUser = \App\Models\User::select($flag)->orderBy($flag)->first();
        return \Carbon\Carbon::parse($firstUser->$flag)->diffInDays();
    });
}

/**
 * How much users was registered.
 *
 * @param \Carbon\Carbon $date
 * @return int
 * @throws
 *
 * http://demo.hyipium.com/admin/see_integration_example/getTotalAccounts
 */
function getTotalAccounts(\Carbon\Carbon $date = null): int
{
    return cache()->tags('totalAccounts')->remember('i.totalAccounts.date-' . $date, now()->addHour(), function () use ($date) {
        if (null !== $date) {
            return \App\Models\User::where('created_at', '<=', $date->format('Y-m-d') . ' 00:00:01')
                ->where('created_at', '>=', $date->format('Y-m-d') . ' 23:59:29')
                ->count();
        }
        return \App\Models\User::count();
    });
}

/**
 * How many customers has deposits.
 *
 * @param \Carbon\Carbon $date
 * @return int
 * @throws
 *
 * http://demo.hyipium.com/admin/see_integration_example/getActiveAccounts
 */
function getActiveAccounts(\Carbon\Carbon $date = null): int
{
    return cache()->remember('i.activeAccounts', now()->addHour(), function () use ($date) {
        $active = \App\Models\User::query();
        if (null !== $date) {
            $active = $active->where('users.created_at', '>=', $date->format('Y-m-d') . ' 00:00:01')
                ->where('users.created_at', '<=', $date->format('Y-m-d') . ' 23:59:29');
        }

        return $active->whereNotNull('licence_id')
            ->count();
    });
}

/**
 * How much customers have invested to this project.
 *
 * @param boolean $useSymbols
 * @return array
 * @throws
 *
 * http://demo.hyipium.com/admin/see_integration_example/getTotalDeposited
 */
function getTotalDeposited($useSymbols = false)
{
    return cache()->remember('i.totalDeposited.useSymbols-' . ($useSymbols ? 'y' : 'n'), now()->addHour(), function () use ($useSymbols) {
        $totalDeposited = [];
        $currencies = getCurrencies();
        $type = \App\Models\TransactionType::getByName('enter');

        foreach ($currencies as $currency) {
            $invested = \App\Models\Transaction::where('currency_id', $currency['id'])
                ->where('type_id', $type->id)
                ->where('approved', 1)
                ->sum('amount');
            $arrayKey = true === $useSymbols ? $currency['symbol'] : $currency['code'];

            if (!isset($totalDeposited[$arrayKey])) {
                $totalDeposited[$arrayKey] = 0;
            }

            $totalDeposited[$arrayKey] += round($invested, $currency['precision']);
        }
        return $totalDeposited;
    });
}

/**
 * How much was withdrew from the project.
 *
 * @param boolean $useSymbols
 * @return array
 * @throws
 *
 * http://demo.hyipium.com/admin/see_integration_example/getTotalWithdrew
 */
function getTotalWithdrew($useSymbols = false)
{
    return cache()->remember('i.totalWithdrew.useSymbols-' . ($useSymbols ? 'y' : 'n'), now()->addHour(), function () use ($useSymbols) {
        $totalWithdrew = [];
        $currencies = getCurrencies();

        foreach ($currencies as $currency) {
            $amount = \App\Models\Withdraw::where('withdraws.status_id', \App\Models\TransactionStatus::STATUS_APPROVED)
                ->where('withdraws.currency_id', $currency['id'])
                ->sum('amount');

            $arrayKey = true === $useSymbols ? $currency['symbol'] : $currency['code'];

            if (!isset($totalWithdrew[$arrayKey])) {
                $totalWithdrew[$arrayKey] = 0;
            }

            $totalWithdrew[$arrayKey] += round($amount, $currency['precision']);
        }
        return $totalWithdrew;
    });
}



function getTotalSell($useSymbols = false)
{
    return cache()->remember('i.totalSell.useSymbols-' . ($useSymbols ? 'y' : 'n'), now()->addMinutes(30), function () use ($useSymbols) {
        $totalWithdrew = [];
        $currencies = getCurrencies();

        foreach ($currencies as $currency) {
            $amount = \App\Models\Transaction::join('transaction_types', function ($join) {
                $join->on('transactions.type_id', '=', 'transaction_types.id');
            });

            $amount = $amount->whereIn('transaction_types.name', ['exchange_sell', 'exchanger_sell'])
                ->where('transactions.approved', 1)
                ->where('transactions.currency_id', $currency['id'])
                ->sum('amount');

            $arrayKey = true === $useSymbols ? $currency['symbol'] : $currency['code'];

            if (!isset($totalWithdrew[$arrayKey])) {
                $totalWithdrew[$arrayKey] = 0;
            }

            $totalWithdrew[$arrayKey] += round($amount, $currency['precision']);
        }
        return $totalWithdrew;
    });
}



function getTypes()
{
    return [
        \App\Models\TransactionType::getByName('add_accelerators')->id =>'',
    ];
}

/**
 * All system currencies.
 *
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getCurrencies
 */
function getCurrencies()
{
    return cache()->remember('i.currencies', getCacheILifetime('currencies'), function () {
        return \App\Models\Currency::get()->map(function($item) {
            return $item->toArray();
        });
    });
}


function getRatesStatics()
{

    $data =  cache()->remember('i.cur_rates', 60, function () {
        $data = [];
        $rates = \App\Models\RateStatistic::orderBy('created_at')->get();
        foreach ($rates as $rate) {
            $data[] = [
                "x"=>\Carbon\Carbon::parse($rate->created_at)->timestamp*1000,
                "y"=>[$rate->open, $rate->high, $rate->low, $rate->close]
            ];
        }

        return $data;
    });




    return json_encode($data);
}



function getExchangeOperations()
{
    $types = 'exchange_buy,exchange_sell';
    return $operations = cache()->tags('userAllOperationsAll')->remember('c.userAllOperationsExchangeAll1.type-' . $types, now()->addMinutes(30), function () use ($types) {

        $operations = Transaction::query();

        if (null !== $types) {
            $ids = [];
            foreach (explode(',', $types) as $type) {
                $typeId = TransactionType::getByName($type);
                $ids[] = $typeId->id;
            }




            if (!empty($ids)) {
                $operations = $operations->whereIn('type_id', $ids);
            }
        }

        return $operations
            ->with('currency', 'type')
            ->orderBy('created_at', 'desc')
            ->limit(10)
            ->get();
    });
}
/**
 * Get all tariff plans.
 *
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getTariffPlans
 */
function getTariffPlans()
{
    return cache()->remember('i.tariffPlans', getCacheILifetime('tariffPlans'), function () {
        return \App\Models\Rate::with([
            'currency'
        ])
            ->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * Get information about all affiliate levels.
 *
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getAffiliateLevels
 */
function getAffiliateLevels()
{
    return cache()->remember('i.affiliateLevels', getCacheILifetime('affiliateLevels'), function () {
        return \App\Models\Referral::get()->map(function($item) {
            return $item->toArray();
        });
    });
}

/**
 * Get online users with some period.
 *
 * @return int
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getVisitorsOnline
 */
function getVisitorsOnline(): int
{
    return cache()->remember('i.visitorsOnline', getCacheILifetime('visitorsOnline'), function () {
        $onlinePeriod = 10; // minutes
        return \App\Models\PageViews::select('user_ip')
            ->distinct()
            ->where('created_at', '>', now()->subMinutes($onlinePeriod))
            ->count(['user_ip']);
    });
}

/**
 * Get online authorized users for some period.
 *
 * @return int
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getMembersOnline
 */
function getMembersOnline(): int
{
    return cache()->remember('i.membersOnline', getCacheILifetime('membersOnline'), function () {
        $onlinePeriod = 10; // minutes
        return \App\Models\PageViews::select('user_ip')
            ->distinct()
            ->where('created_at', '>', now()->subMinutes($onlinePeriod))
            ->whereNotNull('user_id')
            ->count(['user_ip']);
    });
}

/**
 * Last transaction datetime of update.
 *
 * @return \Carbon\Carbon
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getLastUpdate
 */
function getLastUpdate(): \Carbon\Carbon
{
    return cache()->remember('i.lastUpdate', getCacheILifetime('lastUpdate'), function () {
        $lastUpdate = \App\Models\Transaction::select('updated_at')->orderBy('created_at', 'desc')
            ->limit(1)
            ->first();
        return !empty($lastUpdate->craeted_at) ? \Carbon\Carbon::parse($lastUpdate->created_at) : \Carbon\Carbon::now();
    });
}

/**
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getAllNews
 */
function getAllNews()
{
    return cache()->remember('i.allNews', getCacheILifetime('allNews'), function () {
        $languages = getLanguagesArray();
        $allNews = [];

        foreach ($languages as $language) {
            $allNews[$language['code']] = \App\Models\NewsLang::where('lang_id', $language['id'])->get()->map(function($item) {
                return $item->toArray();
            });
        }
        return $allNews;
    });
}

/**
 * @param int $limit
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getLastEarnings
 */
function getLastEarnings(int $limit = 10)
{
    return cache()->tags('lastEarnings')->remember('i.lastEarnings.limit-' . $limit, getCacheILifetime('lastEarnings'), function () use ($limit) {
        return \App\Models\Transaction::join('transaction_types', function ($join) {
            $join->on('transactions.type_id', '=', 'transaction_types.id');
        })
            ->with([
                'wallet',
                'currency',
                'paymentSystem',
                'type',
                'user',
            ])
            ->where('transaction_types.name', 'dividend')
            ->orderBy('transactions.created_at', 'desc')
            ->limit($limit)
            ->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * @param int $limit
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getLastWithdraws
 */
function getLastWithdraws(int $limit = 10)
{
    return cache()->tags('lastWithdrawals')->remember('i.lastWithdraws.limit-' . $limit, getCacheILifetime('lastWithdraws'), function () use ($limit) {
        return \App\Models\Withdraw::with([
                'wallet',
                'currency',
                'paymentSystem',
                'type',
                'user',
            ])
            ->where('withdraws.status_id', \App\Models\TransactionStatus::STATUS_APPROVED)
            ->orderBy('withdraws.created_at', 'desc')
            ->limit($limit)
            ->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * @param int $limit
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getLastCreatedDeposits
 */
function getLastCreatedDeposits(int $limit = 10)
{
    return cache()->tags('lastCreatedDeposits')->remember('i.lastCreatedDeposits.limit-' . $limit, getCacheILifetime('lastCreatedDeposits'), function () use ($limit) {
        return \App\Models\Deposit::where('active', 1)
            ->with([
                'transactions',
                'user',
                'rate',
                'currency',
                'wallet',
            ])
            ->orderBy('created_at', 'desc')
            ->limit($limit)
            ->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * @param int $limit
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getLastCreatedMembers
 */
function getLastCreatedMembers(int $limit = 10)
{
    return cache()->tags('lastCreatedMembers')->remember('i.lastCreatedMembers.limit-' . $limit, getCacheILifetime('lastCreatedMembers'), function () use ($limit) {
        return \App\Models\User::with([
            'transactions',
            'wallets',
            'deposits',
        ])
            ->orderBy('created_at', 'desc')
            ->limit($limit)
            ->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * @return string
 * @throws Exception
 */
function getSupportEmail()
{
    \App\Models\Setting::getValue('email');
}

/**
 * @return string
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getAdminEmail
 */
function getAdminEmail(): string
{
    return cache()->remember('i.adminEmail', getCacheILifetime('adminEmail'), function () {
        $admin = \App\Models\User::select('email')
            ->orderBy('created_at')
            ->limit(1)
            ->first();
        return isset($admin->email) && !empty($admin->email) ? $admin->email : '';
    });
}

/**
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getLanguagesArray
 */
function getLanguagesArray()
{
    return cache()->remember('i.languagesArray', getCacheILifetime('languagesArray'), function () {
        return \App\Models\Language::get()->map(function($item) {
            return $item->toArray();
        });
    });
}

/**
 * @param \App\Models\Rate $plan
 * @param int $amount
 * @param int $duration
 * @return float|int
 */
function calculateInvestment(\App\Models\Rate $plan, $amount=0, $duration=0)
{
    $duration = $duration > 0 ? $duration : $plan->duration;
    return $amount / 100 * $plan->daily *  $duration;
}

/**
 * @return string
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getDateOfLaunch
 */
function getDateOfLaunch(): string
{
    return cache()->remember('i.dateOfLaunch', getCacheILifetime('dateOfLaunch'), function () {
        $flag = 'created_at';
        $firstUser = \App\Models\User::select($flag)->orderBy($flag)->first();
        return isset($firstUser->created_at) && !empty($firstUser->created_at) ? $firstUser->created_at : '';
    });
}

/**
 * Customer FAQs
 *
 * @return array
 * @throws Exception
 */
function getFaqsList($limit = 0)
{
    $lang = session()->has('lang')
        ? \App\Models\Language::where('code', session('lang'))->first()
        : \App\Models\Language::getDefault();
    $query = \App\Models\Faq::where('lang_id', $lang->id)
        ->with([
            'lang'
        ]);

    if ($limit>0)
    {
        $query = $query->limit($limit);
    }
    return $query
        ->get()
        ->map(function($item) {
            return $item->toArray();
        });
}

/**
 * Customer Reviews
 *
 * @return array
 * @throws Exception
 */
function getCustomerReviews()
{
    return cache()->remember('i.customerReviews', getCacheILifetime('customerReviews'), function () {
        $lang = session()->has('lang')
            ? \App\Models\Language::where('code', session('lang'))->first()
            : \App\Models\Language::getDefault();

        return \App\Models\Reviews::where('lang_id', $lang->id)
            ->with([
                'lang'
            ])
            ->active()
            ->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * Get partner information, by id which stored in the cookies.
 *
 * @return array
 * @throws
 */
function getPartnerInfoFromCookies()
{

        $partnerId = isset($_COOKIE['partner_id']) ? $_COOKIE['partner_id'] : null;

        if (null === $partnerId) {
            return [];
        }

        return \App\Models\User::where('my_id', $partnerId)->first()->toArray();

}

/**
 * @return array
 * @throws Exception
 */
function getPaymentSystems()
{
    return cache()->remember('i.paymentSystems', getCacheILifetime('paymentSystems'), function () {
        return \App\Models\PaymentSystem::where('connected', 1)->with([
            'currencies'
        ])->get()->map(function($item) {
            return $item->toArray();
        });
    });
}

/**
 * @return float
 * @throws Exception
 */
function getEnterCommission(): float
{
    return cache()->remember('i.enterCommission', getCacheILifetime('enterCommission'), function () {
        $commission = \App\Models\TransactionType::getByName('enter')->commission;
        return null === $commission ? 0 : $commission;
    });
}

/**
 * @return bool
 * @throws Exception
 */
function loginCaptchaCanBeShown(): bool
{
    return true;
}

/**
 * @param null $active
 * @param null $type
 * @param \Carbon\Carbon|null $date_from
 * @param \Carbon\Carbon|null $date_to
 * @return int
 * @throws Exception
 */



function getDepositsCount($active = null, $type = null, \Carbon\Carbon $date_from = null, \Carbon\Carbon $date_to = null, $reprez = null): int
{
    return cache()->tags('depositsCount')->remember('depositsCount.' . ($active ? $active : 'd') . '.date_from-' . $date_from. '.date_from-'.$date_to.'.type-'.$type.'.reprez-'.$reprez, now()->addHours(3), function () use ($active, $date_from, $date_to, $type, $reprez) {
        $deposits = \App\Models\Deposit::select('*');

        if (null !== $active) {
            $deposits = $deposits->where('active', $active == '1' ? true : false);
        }

        if (null !== $type) {
            $deposits = $deposits->where('type', $type == '1' ? 1 : 0);
        }

        if (null !== $date_from&&null !== $date_to) {


            $deposits = $deposits->where('created_at', '>=', $date_from->format('Y-m-d') . ' 00:00:01')
                ->where('created_at', '<=', $date_to->format('Y-m-d') . ' 23:59:59');
        }

        if (null !== $reprez) {
            $deposits = $deposits->whereHas('user', function ($query) use($reprez) {
                $query->where('representative',$reprez);
            });
        }

        return $deposits->count();
    });
}

/**
 * @param \Carbon\Carbon $date
 * @return int
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getActiveDepositsCount
 */
function getActiveDepositsCount(\Carbon\Carbon $date = null): int
{
    return getDepositsCount('yes',null,  $date);
}

/**
 * @param \Carbon\Carbon $date
 * @return int
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getClosedDepositsCount
 */
function getClosedDepositsCount(\Carbon\Carbon $date = null): int
{
    return getDepositsCount('no',null, $date);
}

/**
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getTopPartner
 */
function getTopPartner()
{
    return cache()->remember('topPartner', getCacheILifetime('topPartner'), function () {
        $top = \App\Models\User::topPartner();

        if (null == $top) {
            return [];
        }

        return $top->toArray();
    });
}

/**
 * @return mixed
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getTopPartner
 */
function getTelegramBots()
{
    return cache()->remember('telegramBots', getCacheILifetime('telegramBots'), function () {
        return \App\Models\Telegram\TelegramBots::get()->map(function($item) {
            return $item->toArray();
        });
    });
}



function getFstSold()
{
    return cache()->remember('fstSold', now()->addHour(), function () {
        return \App\Models\OrderPiece::where('type', \App\Models\ExchangeOrder::TYPE_SELL)->sum('amount');
    });
}

/*
 * Authorized user part of helpers
 */

/**
 * @return string
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserId
 */
function getUserId()
{
    return \Auth::user()->id;
}

/**
 * @return string
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserName
 */
function getUserName()
{
    return \Auth::user()->name;
}

/**
 * @return string
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserEmail
 */
function getUserEmail(): string
{
    return \Auth::user()->email;
}

/**
 * @return int
 *
 * http://demo.hyipium.com/admin/see_integration_example/getPartnerId
 */
function getPartnerId()
{
    return \Auth::user()->partner_id;
}

/**
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getPartnerArray
 */
function getPartnerArray()
{
    return cache()->remember('i.' . \Auth::user()->id . '.partnerArray', getCacheILifetime('partnerArray'), function () {
        $partnerId = getPartnerId();

        if ($partnerId > 0) {
            return \App\Models\User::where('my_id', $partnerId)->first()->toArray();
        }
        return [];
    });
}

/**
 * @return string
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserPhone
 */
function getUserPhone()
{
    return \Auth::user()->phone;
}

/**
 * @return string
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserSkype
 */
function getUserSkype()
{
    return \Auth::user()->skype;
}

/**
 * @return string
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserLogin
 */
function getUserLogin()
{
    return \Auth::user()->login;
}

/**
 * @return string
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserCreatedAt
 */
function getUserCreatedAt(): string
{
    return \Auth::user()->created_at;
}

/**
 * @return string
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserUpdatedAt
 */
function getUserUpdatedAt(): string
{
    return \Auth::user()->updated_at;
}

/**
 * @return string
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserRememberToken
 */
function getUserRememberToken()
{
    return \Auth::user()->remember_token;
}

/**
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserAllDeposits
 */
function getUserAllDeposits()
{
    return cache()->tags('userDeposits.' . getUserId())->remember('i.' . getUserId() . '.userAllDeposits', getCacheILifetime('userAllDeposits'), function () {
        return \App\Models\Deposit::where('user_id', \Auth::user()->id)
            ->with([
                'transactions',
                'user',
                'rate',
                'wallet',
                'paymentSystem'
            ])
            ->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserActiveDeposits
 */
function getUserActiveDeposits()
{
    return cache()->tags('userDeposits.' . getUserId())->remember('i.' . getUserId() . '.userActiveDeposits', getCacheILifetime('userActiveDeposits'), function () {
        return \App\Models\Deposit::where('user_id', getUserId())
            ->with([
                'transactions',
                'user',
                'rate',
                'wallet',
                'paymentSystem'
            ])
            ->where('active', 1)
            ->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserClosedDeposits
 */
function getUserClosedDeposits()
{
    return cache()->tags('userDeposits.' . getUserId())->remember('i.' . getUserId() . '.userClosedDeposits', getCacheILifetime('userClosedDeposits'), function () {
        return \App\Models\Deposit::where('user_id', getUserId())
            ->with([
                'transactions',
                'user',
                'rate',
                'wallet',
                'paymentSystem'
            ])
            ->where('active', 0)
            ->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * @param string $type
 * @param int $approved
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserAllOperations
 */
function getUserAllOperations(string $type = null, int $approved = null)
{
    return cache()->tags('userAllOperations.' . getUserId())->remember('i.' . getUserId() . '.userAllOperations.type-' . ($type != null ? $type : 'all') . '.approved-' . ($approved != null ? $approved : 'all'), now()->addHour(), function () use ($type, $approved) {
        $userAllOperations = \App\Models\Transaction::where('transactions.user_id', \Auth::user()->id)
            ->with([
                'wallet',
                'currency',
                'paymentSystem',
                'deposit',
                'type',
                'user'
            ]);

        if (null !== $type) {
            $userAllOperations = $userAllOperations->join('transaction_types', function ($join) {
                $join->on('transactions.type_id', '=', 'transaction_types.id');
            })->where('transaction_types.name', $type);
        }

        if (null !== $approved) {
            $userAllOperations = $userAllOperations->where('approved', $approved);
        }

        return $userAllOperations->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * @return array
 * @throws Exception
 */
function getUserPartnerOperations()
{
    return getUserAllOperations('partner');
}

/**
 * @param string $currencyId
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserWallets
 */
function getUserWallets($currencyId = null)
{
    return cache()->remember('i.' . getUserId() . '.userWallets.currency-'.$currencyId, getCacheILifetime('userWallets'), function () use ($currencyId) {
        $userWallets = \App\Models\Wallet::with([
            'currency',
            'paymentSystem'
        ])->where('user_id', getUserId());

        if (null !== $currencyId) {
            $userWallets = $userWallets->where('currency_id', $currencyId);
        }

        return $userWallets->get()->map(function($item) {
            return $item->toArray();
        });
    });
}


function rate($first,$last)
{
    if(strtolower($first)==strtolower($last))
    {
        return 1;
    }
    $rate = \App\Models\Setting::getValue(strtolower($first).'_to_'.strtolower($last));

    if ($rate===null)
    {
        $nr = \App\Models\Setting::getValue(strtolower($last).'_to_'.strtolower($first));
        if ($nr>0)
        {
            $rate = 1/(float)$nr;
        }
    }
    return (float)$rate;
}

/**
 * @param $code
 * @param null $ps
 * @return mixed
 * @throws Exception
 */

function getUserWallet($code, $ps=null)
{
    return user()->getUserWallet($code, $ps);
}

/**
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserWithdrawRequests
 */
function getUserWithdrawRequests()
{
    return getUserAllOperations('withdraw', 0);
}

/**
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserIps
 */
function getUserIps()
{
    return cache()->remember('i.' . getUserId() . '.userIps', getCacheILifetime('userIps'), function () {
        return \App\Models\PageViews::select('user_ip')
            ->distinct()
            ->where('user_id', getUserId())
            ->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserPageViews
 */
function getUserPageViews()
{
    return cache()->remember('i.' . getUserId() . '.userPagesViews', getCacheILifetime('userPagesViews'), function () {
        return \App\Models\PageViews::where('user_id', \Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->get()
            ->map(function($item) {
                return $item->toArray();
            });
    });
}

/**
 * @param int $level
 * @param bool $json
 * @return array|string
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserReferrals
 */
function getUserReferrals(int $level = 1, bool $json = false)
{
    return cache()->tags('userReferrals.' . getUserId())->remember('i.' . getUserId() . '.' . $level . '.' . (true === $json ? 'json' : 'array') . '.userReferrals', getCacheILifetime('userReferrals'), function () use ($level, $json) {
        return \Auth::user()->getReferralsOnLevel($level, $json);
    });
}

/**
 * @param int $level
 * @param bool $json
 * @return array|string
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserReferrals
 */
function getUserActiveReferrals(int $level = 1, bool $json = false)
{
    return cache()->tags('userReferrals.active.' . getUserId())->remember('i.' . getUserId() . '.' . $level . '.' . (true === $json ? 'json' : 'array') . '.userReferrals', getCacheILifetime('userReferrals'), function () use ($level, $json) {
        return \Auth::user()->getReferralsOnLevel($level, $json, true);
    });
}

/**
 * @param bool $json
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserReferralsTree
 */
function getUserReferralsTree(bool $json = false)
{
    return cache()->tags('userReferrals.' . getUserId())->remember('i.' . getUserId() . '.' . (true === $json ? 'json' : 'array') . '.userReferralsTree', getCacheILifetime('userReferralsTree'), function () use ($json) {
        return \App\Models\User::getReferralsTree(\Auth::user(), $json);
    });
}

/**
 * Get all user balances by currency
 *
 * @param boolean $useSymbols
 * @param string $currencyId
 * @return array
 * @throws Exception
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserBalancesByCurrency
 */
function getUserBalancesByCurrency($useSymbols = false, $currencyId = null)
{
    return cache()->tags('userBalancesByCurrency.' . getUserId())->remember('i.' . getUserId() . '.useSymbols-' . ($useSymbols ? 'y' : 'n') . '.currencyId-' . ($currencyId != null ? $currencyId : 'all') . '.userBalancesByCurrency', getCacheILifetime('userBalancesByCurrency'), function () use ($useSymbols, $currencyId) {
        return \Auth::user()->getBalancesByCurrency($useSymbols, $currencyId);
    });
}

/**
 * @param \App\Models\User $user
 * @param int $level
 * @return mixed
 * @throws Exception
 */
function getUserLevels(\App\Models\User $user, int $level=1)
{
    return cache()->tags('i.getLevels')->remember('i.getLevels.user-'.$user->id, getCacheILifetime('getLevels'), function () use ($user, $level) {
        return $user->getLevels($level);
    });
}

/**
 * @param \App\Models\User $user
 * @param int $level
 * @return mixed
 * @throws Exception
 */
function getUserLevels24h(\App\Models\User $user, int $level=1)
{
    return cache()->tags('i.getLevels24h')->remember('i.getLevels24h.user-'.$user->id, getCacheILifetime('getLevels24h'), function () use ($user, $level) {
        return $user->getLevels24h($level);
    });
}

/*
 * Total function is not collapsed in one, because for the integration we have to separate it. Comfort first.
 */

/**
 * How much was invested to the project by user.
 *
 * @param boolean $useSymbols
 * @param \App\Models\User $user
 * @return array
 * @throws
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserTotalDeposited
 */
function getUserTotalDeposited($useSymbols = false, $user = null)
{
    $user = $user ?? Auth::user();
    return cache()->tags('userTotalDeposited.' . $user->id)->remember('i.' . $user->id . '.userTotalDeposited.useSymbols-' . ($useSymbols ? 'y' : 'n'), now()->addHour(), function () use ($useSymbols, $user) {
        return $user->getTotalByTransactions($useSymbols, 'enter', 1);
    });
}


function getUserTotalByTransactionType($type, $user = null, $useSymbols = false)
{

    if ($user===null)
    {
        $user = user();
    }

    return cache()->tags('userTotalByTransactionType.' . $user->id.'.'.$type)->remember('i.' . $user->id . '.userTotalByTransactionType.useSymbols-' . ($useSymbols ? 'y' : 'n').'.type-'.$type, now()->addHour(), function () use ($useSymbols, $type, $user) {
        return $user->getTotalByTransactions($useSymbols, $type, 1);
    });
}


function getTotalByTransactionType($type, $useSymbols = false)
{
    return cache()->tags('totalByTransactionType')->remember('totalByTransactionType.useSymbols-' . ($useSymbols ? 'y' : 'n').$type, now()->addHour(), function () use ($useSymbols, $type) {

        $total = [];
        $currencies = getCurrencies();

        foreach ($currencies as $currency) {
            $amount = Transaction::join('transaction_types', function ($join) {
                $join->on('transactions.type_id', '=', 'transaction_types.id');
            });

            if (null !== $type) {
                $amount = $amount->where('transaction_types.name', $type);
            }

            $amount = $amount->where('transactions.currency_id', $currency['id'])
                ->sum('amount');
            $arrayKey = true === $useSymbols ? $currency['symbol'] : $currency['code'];

            if (!isset($total[$arrayKey])) {
                $total[$arrayKey] = 0;
            }

            $total[$arrayKey] += round($amount, $currency['precision']);
        }

        return $total;

    });
}







function getTotalByTransactionTypeCurrency($type, $currency)
{
    return cache()->tags('totalByTransactionType')->remember('totalByTransactionType.Currency-' . $currency.$type, now()->addHour(), function () use ($currency, $type) {


        $amount = Transaction::query();
        if (null !== $type) {
            $amount = $amount->where('type_id', TransactionType::getByName($type)->id);
        }


        $amount = $amount->where('currency_id', \App\Models\Currency::getByCode($currency)->id)
            ->sum('amount');



        return $amount;

    });
}

/**
 * How much was withdrawn from the project by user.
 *
 * @param boolean $useSymbols
 * @param \App\Models\User $user
 * @return array
 * @throws
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserTotalWithdrawn
 */
function getUserTotalWithdrawn($useSymbols = false, $user = null)
{
    $user = $user ?? Auth::user();
    return cache()->tags('userTotalWithdrawn.' . $user->id)->remember('i.' . $user->id . '.userTotalWithdrawn.useSymbols-' . ($useSymbols ? 'y' : 'n'), now()->addHour(), function () use ($useSymbols, $user) {
        return $user->getTotalByTransactions($useSymbols, TransactionType::TRANSACTION_TYPE_WITHDRAW, 1);
    });
}

/**
 * How much was earned in the project by user.
 *
 * @param boolean $useSymbols
 * @param \App\Models\User $user
 * @return array
 * @throws
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserTotalEarned
 */
function getUserTotalEarned($useSymbols = false, $user = null)
{
    $user = $user ?? Auth::user();
    return cache()->tags('userTotalEarned.' . $user->id)->remember('i.' . $user->id . '.userTotalEarned.useSymbols-' . ($useSymbols ? 'y' : 'n'), now()->addHour(), function () use ($useSymbols, $user) {
        return $user->getTotalByTransactions($useSymbols, 'dividend', 1);
    });
}

/**
 * How much was earned in the project by user.
 *
 * @param boolean $useSymbols
 * @param \App\Models\User $user
 * @return array
 * @throws
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserTotalEarned
 */
function getUserTotalPartner($useSymbols = false, $user = null)
{
    $user = $user ?? \Auth::user();
    return cache()->tags('UserTotalPartner.' . $user->id)->remember('i.' . $user->id . '.UserTotalPartner.useSymbols-' . ($useSymbols ? 'y' : 'n'), getCacheILifetime('userTotalEarned'), function () use ($useSymbols, $user) {
        return $user->getTotalByTransactions($useSymbols, 'partner', 1);
    });
}


/**
 * How much was earned in the project by user.
 *
 * @param boolean $useSymbols
 * @param \App\Models\User $user
 * @return array
 * @throws
 *
 * http://demo.hyipium.com/admin/see_integration_example/getUserTotalEarned
 */
function getUserTotalBonus($useSymbols = false, $user = null)
{
    $user = $user ?? \Auth::user();
    return cache()->tags('userTotalBonus.' . $user->id)->remember('i.' . $user->id . '.userTotalBonus.useSymbols-' . ($useSymbols ? 'y' : 'n'), getCacheILifetime('userTotalEarned'), function () use ($useSymbols, $user) {
        return $user->getTotalByTransactions($useSymbols, 'bonus', 1);
    });
}

/**
 * @param bool $useSymbols
 * @param \App\Models\User $user
 * @return mixed
 * @throws Exception
 */
function getUserTotalCharity($useSymbols = false, $user = null)
{
    $user = $user ?? \Auth::user();
    return cache()->tags('userTotalCharity.' . $user->id)->remember('i.' . $user->id . '.userTotalCharity.useSymbols-' . ($useSymbols ? 'y' : 'n'), getCacheILifetime('userTotalDeposited'), function () use ($useSymbols, $user) {
        return $user->getTotalByTransactions($useSymbols, 'charity', 1);
    });
}

/**
 * @param bool $useSymbols
 * @param \App\Models\User $user
 * @return mixed
 * @throws Exception
 */
function getUserTotalSendTransfer($useSymbols = false, $user = null) {
    $user = $user ?? \Auth::user();
    return cache()->tags('userTotalSendTransfer.' . $user->id)->remember('i.' . $user->id . '.userTotalSendTransfer.useSymbols-' . ($useSymbols ? 'y' : 'n'), getCacheILifetime('userTotalSendTransfer'), function () use ($useSymbols, $user) {
        return $user->getTotalByTransactions($useSymbols, 'transfer_send', 1);
    });
}

/**
 * @param bool $useSymbols
 * @param \App\Models\User $user
 * @return mixed
 * @throws Exception
 */
function getUserTotalReceivedTransfer($useSymbols = false, $user = null) {
    $user = $user ?? \Auth::user();
    return cache()->tags('userTotalReceivedTransfer.' . $user->id)->remember('i.' . $user->id . '.userTotalSendTransfer.useSymbols-' . ($useSymbols ? 'y' : 'n'), getCacheILifetime('userTotalSendTransfer'), function () use ($useSymbols, $user) {
        return $user->getTotalByTransactions($useSymbols, 'transfer_receive', 1);
    });
}

/**
 * @param $user \App\Models\User
 * @return string
 */
function getUserReferralLink($user = null): string
{
    return route('partner', [
        $user ? $user->my_id : \Auth::user()->my_id
    ]);
}

/**
 * @return null|string
 * @throws Exception
 */
function getD3V3ReferralsTree()
{
    return getAdminD3V3ReferralsTree(getUserId());
}

/**
 * @param \App\Models\Currency $currency
 * @param float $amount
 * @return string
 * @throws Exception
 */
function convertToRub(\App\Models\Currency $currency, float $amount)
{
    switch($currency->code) {
        case 'USD':
            $amount = convertUsdToRub($amount);
            break;

        case 'BTC':
            $amount = convertBtcToRub($amount);
            break;

        case 'LTC':
            $amount = convertLtcToRub($amount);
            break;

        case 'DOGE':
            $amount = convertDogeToRub($amount);
            break;
    }
    return round($amount, 2);
}

/**
 * @param \App\Models\Currency $currency
 * @param float $amount
 * @return string
 * @throws Exception
 */
function convertToUsd(\App\Models\Currency $currency, float $amount)
{
    switch($currency->code) {
        case 'RUR':
            $amount = convertRubToUsd($amount);
            break;

        case 'BTC':
            $amount = convertBtcToUsd($amount);
            break;

        case 'LTC':
            $amount = convertLtcToUsd($amount);
            break;

        case 'DOGE':
            $amount = convertDogeToUsd($amount);
            break;
    }
    return round($amount, 2);
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertBtcToUsd(float $amount)
{
    $rate = \App\Models\Setting::getValue('usd_to_btc');

    if (null === $rate) {
        $rate = 1;
    }

    return $amount / $rate;
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertLtcToUsd(float $amount)
{
    $rate = \App\Models\Setting::getValue('usd_to_ltc');

    if (null === $rate) {
        $rate = 1;
    }

    return $amount / $rate;
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertDogeToUsd(float $amount)
{
    $rate = \App\Models\Setting::getValue('usd_to_btc');

    if (null === $rate) {
        $rate = 1;
    }

    return $amount / $rate;
}

/**
 * @param \App\Models\Currency $currency
 * @param float $amount
 * @return string
 * @throws Exception
 */
function convertRubToOriginal(\App\Models\Currency $currency, float $amount)
{
    switch($currency->code) {
        case 'USD':
            $amount = convertRubToUsd($amount);
            break;

        case 'BTC':
            $amount = convertRubToBtc($amount);
            break;

        case 'LTC':
            $amount = convertRubToLtc($amount);
            break;

        case 'DOGE':
            $amount = convertRubToDoge($amount);
            break;
    }
    return round($amount, $currency->precision);
}

/**
 * @param \App\Models\Currency $currency
 * @param float $amount
 * @return string
 * @throws Exception
 */
function convertUsdToOriginal(\App\Models\Currency $currency, float $amount)
{
    switch($currency->code) {
        case 'RUR':
            $amount = convertUsdToRub($amount);
            break;

        case 'BTC':
            $amount = convertUsdToBtc($amount);
            break;

        case 'LTC':
            $amount = convertUsdToLtc($amount);
            break;

        case 'DOGE':
            $amount = convertUsdToDoge($amount);
            break;
    }
    return round($amount, $currency->precision);
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertUsdToBtc(float $amount)
{
    $rate = \App\Models\Setting::getValue('usd_to_btc');

    if (null === $rate) {
        $rate = 1;
    }

    return $amount * $rate;
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertUsdToLtc(float $amount)
{
    $rate = \App\Models\Setting::getValue('usd_to_ltc');

    if (null === $rate) {
        $rate = 1;
    }

    return $amount * $rate;
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertUsdToDoge(float $amount)
{
    $rate = \App\Models\Setting::getValue('usd_to_doge');

    if (null === $rate) {
        $rate = 1;
    }

    return $amount * $rate;
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertUsdToRub(float $amount)
{
    $rate = \App\Models\Setting::getValue('usd_to_rub');

    if (null === $rate) {
        $rate = 1;
    }

    return $amount * $rate;
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertRubToUsd(float $amount)
{
    $rate = \App\Models\Setting::getValue('usd_to_rub');

    if (null === $rate) {
        $rate = 1;
    }

    return $amount / $rate;
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertBtcToRub(float $amount)
{
    $rate = \App\Models\Setting::getValue('btc_to_rub');

    if (null === $rate) {
        $rate = 1;
    }

    return $amount * $rate;
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertRubToBtc(float $amount)
{
    $rate = \App\Models\Setting::getValue('btc_to_rub');

    if (null === $rate) {
        $rate = 1;
    }

    return $amount / $rate;
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertLtcToRub(float $amount)
{
    return $amount * \App\Models\Setting::getValue('ltc_to_rub'); // 2015.87
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertRubToLtc(float $amount)
{
    return $amount / \App\Models\Setting::getValue('ltc_to_rub'); // 2015.87
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertDogeToRub(float $amount)
{
    return $amount * \App\Models\Setting::getValue('doge_to_rub'); // 0.144652
}

/**
 * @param float $amount
 * @return float|int
 * @throws Exception
 */
function convertRubToDoge(float $amount)
{
    return $amount / \App\Models\Setting::getValue('doge_to_rub'); // 0.144652
}

/**
 * @param $amount
 * @return float
 */
function toStrFloat($amount)
{
    return (float) $amount;
}