<?php

namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 * @package App\Models
 *
 * @property string id
 * @property string type_id - тип операции
 * @property string user_id
 * @property string rate_id - тарифный план, если это депозитная транзакция.
 * @property string deposit_id
 * @property string wallet_id
 * @property string payment_system_id
 * @property float amount
 * @property string source - кошелек реферала пользователя, если это партнерская транзакция.
 * @property string result - ответ платежной системы.
 * @property string batch_id - ИД операции в платежной системе.
 * @property bool approved
 * @property ExchangeOrder order
 * @property float commission
 * @property integer check
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property float rate
 * @property string confirmation_code
 * @property Carbon confirmation_sent_at
 */
class Transaction extends Model
{
    use Uuids;
    use ModelTrait;

    /** @var bool $incrementing */
    public $incrementing = false;
    protected $keyType = 'string';

    /** @var array $fillable */
    protected $fillable = [
        'type_id',
        'user_id',
        'currency_id',
        'rate_id',
        'deposit_id',
        'wallet_id',
        'payment_system_id',
        'amount',
        'source',
        'result',
        'order_id',
        'batch_id',
        'approved',
        'commission',
        'created_at',
        'check',
        'rate',
        'confirmation_code',
        'confirmation_sent_at'
    ];

    protected $dates = [
        'confirmation_sent_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(ExchangeOrder::class, 'order_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rank()
    {
        return $this->belongsTo(UserRank::class, 'source', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deposit()
    {
        return $this->belongsTo(Deposit::class, 'deposit_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }



    public static function set_rank($wallet, $source="")
    {
        $type = TransactionType::getByName('set_rank');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => 0,
            'source' =>$source,
            'approved' => true,
        ]);
        return $transaction->save() ? $transaction : null;
    }

    public static function createDeposit($deposit)
    {
        $type = TransactionType::getByName('create_dep');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => 0,
            'user_id' => $deposit->user->id,
            'currency_id' => $deposit->wallet->currency->id,
            'rate_id' => $deposit->rate->id,
            'deposit_id' => $deposit->id,
            'wallet_id' => $deposit->wallet_id,
            'payment_system_id' => $deposit->wallet->paymentSystem->id,
            'amount' => $deposit->balance,
            'result'=>$deposit->wallet->balance
        ]);
        return $transaction->save() ? $transaction : null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentSystem()
    {
        return $this->belongsTo(PaymentSystem::class, 'payment_system_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(TransactionType::class, 'type_id', 'id');
    }




    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @param $value
     * @return float
     * @throws \Exception
     */
    public function getAmountAttribute($value)
    {
        if (null == $this->currency_id) {
            return $value;
        }

        return currencyPrecision($this->currency_id, $value);
    }

    /**
     * @param $wallet
     * @param $amount
     * @return mixed
     */
    public static function enter($wallet, $amount)
    {
        $type = TransactionType::getByName('enter');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
        ]);
        return $transaction->save() ? $transaction : null;
    }

    public static function enter_exchange($wallet, $amount, $currency, $ps)
    {
        $type = TransactionType::getByName('enter_exchange');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $ps->id,
            'amount' => $amount,

            'approved'=>true
        ]);
        return $transaction->save() ? $transaction : null;
    }


    public static function addFst($wallet, $amount, $deposit)
    {
        $type = TransactionType::getByName('add_fst');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency_id,
            'deposit_id' => $deposit->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->payment_system_id,
            'amount' => $amount,
        ]);
        return $transaction->save() ? $transaction : null;
    }


    public static function order_stash($wallet, $amount, $order)
    {
        $type = TransactionType::getByName('order_stash');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
            'order_id' => $order->id,
            'approved' => true,
        ]);
        return $transaction->save() ? $transaction : null;
    }


    public static function licence_cash_back($wallet, $amount, $id)
    {
        $type = TransactionType::getByName('licence_cash_back');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
            'approved' => true,
            'source'=>$id
        ]);
        return $transaction->save() ? $transaction : null;
    }


    public static function charity($wallet, $amount)
    {
        $type = TransactionType::getByName('charity');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
            'approved' => true,
        ]);

        $wallet->update([
            'balance' => $wallet->balance - $amount
        ]);

        return $transaction->save()
            ? $transaction
            : null;
    }


    public static function buy_license($wallet, $amount, $id)
    {
        $type = TransactionType::getByName('buy_license');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
            'approved' => true,
            'source'=>$id
        ]);

        $wallet->update([
            'balance' => $wallet->balance - $amount
        ]);


        return $transaction->save()
            ? $transaction
            : null;
    }

    /**
     * Create cashback transaction
     * @param $wallet
     * @param $amount
     * @param $cashbackRequestId
     * @return mixed
     */
    public static function cashback($wallet, $amount, $cashbackRequestId)
    {
        $type = TransactionType::getByName('cashback');

        return self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user_id,
            'currency_id' => $wallet->currency_id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->payment_system_id,
            'amount' => $amount,
            'approved' => true,
            'source'=>$cashbackRequestId
        ]);
    }


    public static function buy_wec($wallet, $usd_wallet ,$amount, $rate)
    {
        $type = TransactionType::getByName('buy_wec');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
            'approved' => true,
            'source'=>$rate
        ]);

        $wallet->update([
            'balance' => $wallet->balance + $amount
        ]);

        $usd_wallet->update([
            'balance' => $usd_wallet->balance - $amount*$rate
        ]);


        return $transaction->save()
            ? $transaction
            : null;
    }

    /**
     * @param Wallet $wallet
     * @param $amount
     * @param array $transactionData
     * @return Transaction|null
     */
    public static function bonus(Wallet $wallet, $amount, array $transactionData = []): ?Transaction
    {
        $type = TransactionType::getByName('bonus');
        $transaction = self::create([
            'type_id'           => $type->id,
            'commission'        => $type->commission,
            'user_id'           => $wallet->user_id,
            'currency_id'       => $wallet->currency_id,
            'wallet_id'         => $wallet->id,
            'payment_system_id' => $wallet->payment_system_id,
            'amount'            => $amount,
            'source'            => $transactionData['source'] ?? null,
            'batch_id'          => $transactionData['batch_id'] ?? null,
            'approved'          => true
        ]);
        return $transaction->save() ? $transaction : null;
    }


    public static function exchange_commission($wallet, $amount)
    {
        $type = TransactionType::getByName('exchange_commission');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
            'approved' => true,
        ]);
        return $transaction->save() ? $transaction : null;
    }

    /**
     * @param $order
     * @param $wallet
     * @param $amount
     * @return null
     */
    public static function createExchangeOrder($order, $wallet, $amount = null)
    {

        $type = TransactionType::getByName('exchange_order');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => 0,
            'user_id' => $order->user->id,
            'currency_id' => $wallet->currency->id,
            'order_id' => $order->id,
            'source'=>rate('ACC', 'USD'),
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount ? $amount : $order->amount,
            'approved' => true,
        ]);
        return $transaction->save() ? $transaction : null;
    }


    /**
     * @param $type
     * @param Wallet $wallet
     * @param float $amount
     * @param ExchangeOrder $order
     * @return |null
     */
    public static function exchange_any($type, Wallet $wallet, float $amount, ExchangeOrder $order, $rate, $commission=0)
    {
        $amount = (float)abs($amount);
        /** @var TransactionType $type */
        $type = TransactionType::getByName('exchange_' . $type);
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
            'order_id' => $order->id,
            'approved' => true,
            'source'=>$rate,
            'result' => $wallet->balance
        ]);
        return $transaction->save() ? $transaction : null;
    }


    public static function exchanger_any($type, Wallet $wallet, float $amount, ExchangeOrder $order, $rate, $commission=0)
    {
        $amount = (float)abs($amount);
        /** @var TransactionType $type */
        $type = TransactionType::getByName('exchanger_' . $type);
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
            'order_id' => $order->id,
            'approved' => true,
            'source'=>$rate,
            'result' => $wallet->balance
        ]);
        return $transaction->save() ? $transaction : null;
    }


    /**
     * @param Wallet $wallet
     * @param float $amount
     * @param ExchangeOrder $order
     * @return |null
     */
    public static function exchange_close_order(Wallet $wallet, float $amount, ExchangeOrder $order)
    {
        $amount = (float)abs($amount);
        /** @var TransactionType $type */
        $type = TransactionType::getByName('exchange_close_order');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => $type->commission,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
            'order_id' => $order->id,
            'approved' => true,
        ]);
        return $transaction->save() ? $transaction : null;
    }

    /**
     * @param $wallet
     * @param $amount
     * @param $referral
     * @return null
     */
    public static function partner($wallet, $amount, $referral, $level)
    {
        $type = TransactionType::getByName('partner');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => 0,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
            'source' => $referral->id,
            'result'=>$level,
            'approved' => true,
        ]);
        return $transaction->save() ? $transaction : null;
    }





    /**
     * @param $wallet
     * @param $amount
     * @param null $referral
     * @param Deposit|null $deposit
     * @return |null
     */
    public static function dividend($wallet, $amount, $referral = null, Deposit $deposit = null)
    {
        $type = TransactionType::getByName('dividend');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => 0,
            'deposit_id' => $deposit->id,
            'user_id' => $wallet->user->id,
            'currency_id' => $wallet->currency->id,
            'wallet_id' => $wallet->id,
            'payment_system_id' => null,
            'amount' => $amount,
            'source' => null !== $referral
                ? $referral->id
                : null,
            'approved' => true,
        ]);

        return $transaction->save() ? $transaction : null;

    }


    /**
     * @param $deposit
     * @param $amount
     * @return null
     */
    public static function closeDeposit($deposit, $amount)
    {
        $type = TransactionType::getByName('close_dep');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => 0,
            'user_id' => $deposit->user->id,
            'currency_id' => $deposit->wallet->currency_id,
            'rate_id' => $deposit->rate->id,
            'deposit_id' => $deposit->id,
            'wallet_id' => $deposit->wallet->id,
            'payment_system_id' => $deposit->paymentSystem->id,
            'amount' => $amount,
        ]);
        return $transaction->save() ? $transaction : null;
    }

    /**
     * @param $wallet
     * @param $amount
     * @param $source
     * @param $approved
     * @return null
     */
    public static function penalty($wallet, $amount, $source = null, $approved = false)
    {
        $type = TransactionType::getByName('penalty');
        $transaction = self::create([
            'type_id' => $type->id,
            'commission' => 0,
            'user_id' => $wallet->user_id,
            'currency_id' => $wallet->currency->id,
            'rate_id' => null,
            'deposit_id' => null,
            'wallet_id' => $wallet->id,
            'source' => $source,
            'payment_system_id' => $wallet->paymentSystem->id,
            'amount' => $amount,
            'approved' => $approved
        ]);
        return $transaction->save() ? $transaction : null;
    }



    /**
     * @param string $type
     * @param string $role
     * @return array
     * @throws \Exception
     */
    public static function transactionBalances(string $type, string $role = ''): array
    {
        $type = TransactionType::getByName($type);

        if ($role) {
            $transactions = User::role($role)->join('transactions', function ($join) use ($type) {
                $join->on('users.id', '=', 'transactions.user_id')
                    ->where('transactions.approved', true)->where('transactions.type_id', $type->id);
            })->join('currencies', 'currencies.id', '=',
                'transactions.currency_id')->select('currencies.code', 'transactions.amount')->get();
        } else {
            $transactions = Currency::join('transactions', function ($join) use ($type) {
                $join->on('currencies.id', '=', 'transactions.currency_id')
                    ->where('transactions.approved', true)->where('transactions.type_id', $type->id);
            })->select('currencies.code', 'transactions.amount')->get();
        }

        $balances = Currency::balances();

        foreach ($transactions as $item) {
            $balances[$item->code] = key_exists($item->code, $balances)
                ? $balances[$item->code] + $item->amount
                : $item->amount;
        }

        return $balances;

    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function commissionBalances(): array
    {
        $balances = [];
        $bonus = Transaction::transactionBalances('bonus');
        $enter = Transaction::transactionBalances('enter');
        $withdraw = Transaction::transactionBalances('withdraw'); // Moved to Model withdraws

        foreach (Currency::all() as $currency) {
            $balances[$currency->code] = $bonus[$currency->code] * TransactionType::getByName('bonus')->commission * 0.01 + $enter[$currency->code] * TransactionType::getByName('enter')->commission * 0.01 + $withdraw[$currency->code] * TransactionType::getByName('withdraw')->commission * 0.01;
        }
        return $balances;
    }

    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->approved == 1;
    }
}
