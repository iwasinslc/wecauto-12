<?php
namespace App\Rules;

use App\Models\ExchangeOrder;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

/**
 * Class RuleHasPhone
 * @package App\Rules
 */
class RuleHasLicence implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $mainWallet = user()->wallets()->find(request()->main_wallet_id);
        if (request()->type==ExchangeOrder::TYPE_BUY&&$mainWallet->currency->code!='FST')
        {
            return true;
        }
        return user()->activeLicence();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('You do not have an active license.');
    }
}
