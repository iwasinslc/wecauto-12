<?php
namespace App\Rules;

use App\Models\Currency;
use App\Models\Licences;
use App\Models\Wallet;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\Rate;

/**
 * Class RuleRateCurrency
 * @package App\Rules
 */
class RuleLicenceCurrency implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /**
         * @var Licences $licence
         */
        $licence = Licences::find($value);
        /**
         * @var Wallet $wallet
         */

        $user = user();
        $wallet = $user->wallets()->find(request()->wallet_id);

        // Allowed PZM and WEC
        return $wallet && in_array($wallet->currency_id,  [
            $licence->currency_id
//            Currency::getByCode('PZM')->id
        ]);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.rate_currency');
    }
}
