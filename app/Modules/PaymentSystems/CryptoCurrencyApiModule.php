<?php
namespace App\Modules\PaymentSystems;

use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Models\User;
use App\Models\Withdraw;


/**
 * Class CryptoCurrencyApiModule
 * @package App\Modules\PaymentSystems
 */
class CryptoCurrencyApiModule
{
//    /**
//     * @return array
//     * @throws \Exception
//     */
    public static function getBalances(): array
    {
        $ps       = PaymentSystem::getByCode('cryptocurencyapi');
        $balances = [];

        foreach ($ps->currencies as $currency) {
            try {
                $balances[$currency->code] = self::getBalance($currency->code);
            } catch (\Exception $exception) {
                throw new \Exception($exception->getMessage());
            }
        }

        if (count($balances) > 0 && !empty($ps)) {
            $ps->update([
                'external_balances' => json_encode($balances),
                'connected' => true,
            ]);
        } else {
            $ps->update([
                'external_balances' => json_encode([]),
                'connected' => false,
            ]);
            throw new \Exception('Balance is not reachable.');
        }

        return $balances;
    }
    
    public static function transfer(Withdraw $transaction
    ) {


        /** @var Wallet $wallet */
        $wallet         = $transaction->wallet()->first();
        /** @var User $user */
        $user           = $wallet->user()->first();
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem  = $transaction->paymentSystem()->first();
        /** @var Currency $currency */
        $currency       = $transaction->currency()->first();

        if (null === $wallet || null === $user || null === $paymentSystem || null === $currency) {
            throw new \Exception('Wallet, user, payment system or currency is not found for withdrawal approve.');
        }

        $data = [
            'address'=>$transaction->source,
            'currency'=>$currency->code,
            'amount'=>$transaction->amount-$transaction->commission
        ];

        $result = self::request('GET','send', $data);

        if (isset($result->error)) {
            throw new \Exception('Can not withdraw '.$transaction->amount.$currency->symbol.'. Reason: '.$result->error);
        }



        return $result->result;
    }

    /**
     * @param string $currency
     * @return float
     * @throws \Exception
     */
    public static function getBalance(string $currency)
    {
        $currency = Currency::where('code', $currency)->first();

        $result = self::request('GET','balance', [
            'currency'=>$currency->code
        ]);
//        var_dump($currency->code);
//        var_dump($result);

        \Log::critical(print_r($result->result, true));

        return $result->result;
    }

    /**
     * @param Transaction $transaction
     * @return mixed
     * @throws \Exception
     */
    public static function createTopupTransaction(Transaction $transaction)
    {



        $req = [
            'amount'      => $transaction->amount,
            'currency'   => strtoupper($transaction->currency->code),
            'address'     => env('CC_API_ADDRESS'), // leave blank send to follow your settings on the Coin Settings page
            'tag'     => $transaction->id,
        ];


        $result = self::request('GET','track', $req);

        if (isset($result->error) ) {
            throw new \Exception($result->error);
        }

        return $result->result;
    }

    /**
     * @param string $currency
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

    public static function getAddress(string $currency)
    {
        $currency = Currency::where('code', $currency)->first();

        $result = self::request('GET','give', [
            'currency'=>$currency->code
        ]);


        \Log::critical(print_r($result, true));

        return $result->result;
    }


    /**
     * @param string $method
     * @param string $address
     * @param array $data
     * @param array|null $additionHeaders
     * @return object
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function request($method, $address, $data=[], $additionHeaders=null)
    {
        $client   = new \GuzzleHttp\Client();
        $headers  = [
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json',
        ];
        $params   = [
            'headers' => array_merge($headers, (is_array($additionHeaders) ? $additionHeaders : [])),
            'verify'  => false,
            //'json'    => $data,
        ];

        $fields = http_build_query($data, '', '&');

        try {
            $response = $client->request($method, 'https://cryptocurrencyapi.net/api/.'.$address.'?key='.env('CC_API_KEY').'&'.$fields, $params);
        } catch (\Exception $e) {
            throw new \Exception('Request to '.$address.' is failed. '.$e->getMessage());
        }

        if ($response->getStatusCode() !== 200) {
            throw new \Exception('Request to '.$address.' was with response status '.$response->getStatusCode());
        }

        return json_decode($response->getBody()->getContents());
    }
}