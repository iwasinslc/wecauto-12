<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Deposit;
use App\Models\DepositQueue;
use App\Models\Rate;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;

/**
 * Class DepositController
 * @package App\Http\Controllers\Admin
 */
class WalletsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.wallets.index');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function dataTable()
    {
        $wallets = Wallet::with('user', 'currency', 'paymentSystem')
            ->select('wallets.*');

        return Datatables::of($wallets)
            ->make(true);
    }

}
