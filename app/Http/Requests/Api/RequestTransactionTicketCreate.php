<?php

namespace App\Http\Requests\Api;

use App\Rules\RuleUUIDEqual;
use App\Rules\RuleWalletExist;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="RequestTransactionTicketCreate",
 *      description="Create transaction request body data",
 *      type="object",
 *      required={"wallet_id","value","comment"},
 *      @OA\Property(property="wallet_id",ref="#/components/schemas/Wallet/properties/id"),
 *      @OA\Property(property="value",ref="#/components/schemas/Transaction/properties/value"),
 *      @OA\Property(
 *          title="Batch ID",
 *          property="batch_id",
 *          description="Batch id",
 *          type="string"
 *      ),
 *     @OA\Property(
 *          title="Comment",
 *          property="comment",
 *          description="User's comment, why this transaction was created",
 *          type="string"
 *      )
 *
 * )
 */
class RequestTransactionTicketCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wallet_id' => [
                'required',
                new RuleUUIDEqual()
            ],
            'value' => 'required|numeric',
            'batch_id' => 'string|nullable|max:191',
            'comment' => 'required|string|max:191'
        ];
    }
}
