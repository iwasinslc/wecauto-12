<?php


namespace App\Services;


use App\Events\NotificationEvent;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Support\Facades\DB;

class TransferService extends TransactionService
{
    /**
     * @param Wallet $sender_wallet
     * @param Wallet $receiver_wallet
     * @param float $amount
     * @param bool $with_confirmation
     * @throws
     */
    public function create(
        Wallet $sender_wallet,
        Wallet $receiver_wallet,
        float $amount,
        $with_confirmation = true
    ) {
        $amount = (float) abs($amount);
        /** @var TransactionType $type */
        $send_type           = TransactionType::getByName('transfer_send');
        $receive_type           = TransactionType::getByName('transfer_receive');
        /** @var User $sender */
        $sender           = $sender_wallet->user()->first();
        /** @var Currency $currency */
        $currency       = $sender_wallet->currency()->first();
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem  = $sender_wallet->paymentSystem()->first();
        $receiver = $receiver_wallet->user()->first();

        if($with_confirmation) {
            $confirmation_code = $this->getConfirmationCode();
            $confirmation_sent_at = now();
        }

        DB::beginTransaction();
        try {
            $transaction = Transaction::create([
                'type_id'           => $send_type->id,
                'commission'        => 0,
                'user_id'           => $sender->id,
                'currency_id'       => $currency->id,
                'wallet_id'         => $sender_wallet->id,
                'payment_system_id' => $paymentSystem->id,
                'amount'            => $amount,
                'approved'          => false,
                'result'            => $receiver->email,
                'confirmation_code' => $confirmation_code ?? null,
                'confirmation_sent_at' => $confirmation_sent_at ?? null
            ]);

            $receiver_transaction = Transaction::create([
                'type_id'           => $receive_type->id,
                'commission'        => 0,
                'user_id'           => $receiver->id,
                'currency_id'       => $currency->id,
                'wallet_id'         => $receiver_wallet->id,
                'payment_system_id' => $paymentSystem->id,
                'amount'            => $amount,
                'approved'          => false,
                'result'            => $sender->email,
                'confirmation_code' => $confirmation_code ?? null,
                'confirmation_sent_at' => $confirmation_sent_at ?? null
            ]);

            /*User::notifyAdminsViaNotificationBot('new_withdrawal', [
                'transaction' => $transaction,
            ]);*/

            if($with_confirmation) {
                $this->sendConfirmation($sender, [
                    'subject' => 'Transfer confirmation',
                    'type' => self::TYPE_TRANSFER,
                    'confirmation_code' => $confirmation_code
                ]);
            } else { // Confirm transfer without confirmation by email
                $this->confirm([$transaction, $receiver_transaction], $with_confirmation);
            }
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Confirm transaction
     * @param Transaction[] $transactions
     * @param $with_confirmation
     * @throws
     */
    public function confirm($transactions, $with_confirmation = false) {
        $transfer_send_type = TransactionType::getByName('transfer_send');
        $transfer_receive_type = TransactionType::getByName('transfer_receive');

        DB::beginTransaction();
        try {
            foreach($transactions as $transaction) {
                $wallet = $transaction->wallet()->lockForUpdate()->first();

                $commission           = (float)Setting::getValue('commission_'.strtolower($transaction->currency->code));
                $commission = ($commission*$transaction->amount)/100;
                $amountWithCommission = $transaction->amount - $commission;
                switch ($transaction->type_id) {
                    case $transfer_send_type->id: // Credit balance
                        if($with_confirmation) {
                            $this->checkTransactionExpiration($transaction); // Only if with confirmation
                        }
                        $this->checkBalance($transaction, $wallet);
                        $wallet->update([
                            'balance' => $wallet->balance -  $transaction->amount
                        ]);
                        $transaction->commission = $commission;
                        $transaction->save();
                        break;
                    case $transfer_receive_type->id: // Debit balance
                        $wallet->update([
                            'balance' => $wallet->balance +  $amountWithCommission
                        ]);
                        break;
                    default:
                        throw new \Exception('Unknown transaction type');
                }
                $transaction->approved = 1;
                $transaction->save();
            }
            // Send notification
            NotificationEvent::dispatch($transactions[0]->user, 'notifications.transfer_confirmed', []);
            // Commit changes
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param $confirmation_code
     * @return array|string|null
     * @throws \Exception
     */
    public function confirmByCode($confirmation_code) {
        $transactions = Transaction::where('confirmation_code', $confirmation_code)
            ->where('approved', false)
            ->get();
        if($transactions->count() == 0) {
            throw new \Exception('Transfer not found');
        }
        $this->confirm($transactions, true);
        return __('Transfer confirmed successfully');
    }

}