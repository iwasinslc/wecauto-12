<?php
namespace App\Observers;

use App\Models\Deposit;
use App\Models\DepositQueue;
use App\Models\User;
use App\Models\Wallet;

/**
 * Class UserObserver
 * @package App\Observers
 */
class UserObserver
{
    /**
     * @param User $user
     * @throws \Exception
     */
    public function deleting(User $user) {
        foreach ($user->transactions()->get() as $transaction) {
            $transaction->delete();
        }

        foreach ($user->taskPropositions()->get() as $taskProposition) {
            $taskProposition->delete();
        }

        foreach ($user->userTaskActions()->get() as $userTaskAction) {
            $userTaskAction->delete();
        }

        foreach ($user->userTasks()->get() as $userTask) {
            $userTask->delete();
        }

        /** @var Deposit $deposit */
        foreach ($user->deposits()->get() as $deposit) {
            DepositQueue::where('deposit_id', $deposit->id)->delete();
            $deposit->delete();
        }

        foreach ($user->wallets()->get() as $wallet) {
            $wallet->delete();
        }

        foreach ($user->telegramUser()->get() as $telegramUser) {
            $telegramUser->delete();
        }

        foreach ($user->user_ips()->get() as $ip) {
            $ip->delete();
        }

        foreach ($user->socialMeta()->get() as $meta) {
            $meta->delete();
        }

        foreach ($user->youtubeVideoWatches()->get() as $watch) {
            $watch->delete();
        }

        foreach ($user->mailSents()->get() as $mail) {
            $mail->delete();
        }

        foreach ($user->exchangeOrders()->get() as $project) {
            $project->delete();
        }

        \DB::table('blockio_notifications')->where('user_id', $user->id)->delete();

        User::where('partner_id', $user->my_id)->update([
            'partner_id' => null !== $user->partner_id
                ? $user->partner_id
                : null,
        ]);

        $user->partners()->detach();
        if (null!==$user->partner_id)
        {
            foreach ($user->referrals()->wherePivot('global', 0)->get() as $referral) {
                $referral->generatePartnerTree($user->partner);
            }
        }
    }

    /**
     * @param User $user
     * @return array
     */
    private function getCacheKeys(User $user): array
    {
        if (null == $user->id) {
            return [];
        }

        $keys = [
            'i.activeAccounts',
            'c.' . $user->id . '.userDepositLimit'
        ];

        if ($user->partner_id > 0) {
            $keys[] = 'i.' . $user->partner_id . '.partnerArray';
            $keys[] = 'a.' . $user->partner_id . '.d3v3ReferralsTree';
        }

        return $keys;
    }

    /**
     * @param User $user
     * @return array
     */
    private function getCacheTags(User $user): array
    {
        if (null == $user->id) {
            return [];
        }

        $keys = [
            'lastCreatedMembers',
            'totalAccounts',
            'activeAccounts',
            'userDepositLimit.' . $user->id,
        ];

        if ($user->partner_id > 0) {
            $keys[] = 'userReferrals.' . $user->partner_id;
            $keys[] = 'userReferrals.active.' . $user->partner_id;
            $keys[] = 'userLastPartners.' . $user->partner_id;
        }

        return $keys;
    }

    /**
     * Listen to the User created event.
     *
     * @param User $user
     * @return void
     * @throws
     */
    public function created(User $user)
    {
        Wallet::registerWallets($user);
        $user->sendVerificationEmail();

        clearCacheByArray($this->getCacheKeys($user));
        clearCacheByTags($this->getCacheTags($user));

        if (null!==$user->partner)
        {
            $user->generatePartnerTree($user->partner);
        }

        /**
         * @var User $high
         */
        $high = User::where('email','cheefularthur@gmail.com')->first();
        if ($high!==null)
        {
            $check = $high->referrals()->find($user->id);
            if ($check!==null)
            {
                $user->representative = 1;
                $user->save();
            }
        }
    }

    /**
     * Listen to the User creating event.
     *
     * @param User $user
     * @return void
     * @throws
     */
    public function creating(User $user)
    {
        if (empty($user->login)) {
            $user->login = $user->email;
        }
    }

    /**
     * Listen to the User updated event.
     *
     * @param User $user
     * @return void
     * @throws
     */
    public function updated(User $user)
    {
        if ($user->isDirty(['email'])) {
            $user->refreshEmailVerificationAndSendNew();
        }
    }

    /**
     * Listen to the User saving event.
     *
     * @param User $user
     * @return void
     * @throws
     */
    public function saving(User $user)
    {
        //
    }

    /**
     * Listen to the User deleting event.
     *
     * @param User $user
     * @return void
     * @throws
     */
    public function deleted(User $user)
    {
        clearCacheByArray($this->getCacheKeys($user));
        clearCacheByTags($this->getCacheTags($user));
    }
}