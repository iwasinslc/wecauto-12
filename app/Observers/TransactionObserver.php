<?php
namespace App\Observers;

use App\Models\Transaction;
use App\Models\TransactionType;

/**
 * Class TransactionObserver
 * @package App\Observers
 */
class TransactionObserver
{
    /**
     * @param Transaction $transaction
     * @return array
     * @throws
     */
    private function getCacheKeys(Transaction $transaction): array
    {
        if (null == $transaction->user_id) {
            return [];
        }

        $keys = [
            'i.lastUpdate',
            'a.transactionsCount',
        ];

        if ($transaction->type->name == 'withdraw') {
            $keys[] = 'i.totalWithdrew';
            $keys[] = 'a.withdrawRequestsCount';
            $keys[] = 'usersWithdrawals.user-' . $transaction->user_id;
        }


        $types = [
            'exchange_sell',
            'exchanger_sell',
            'exchange_buy',
            'exchanger_buy'
        ];

        if (in_array($transaction->type->name, $types)) {
            $keys[] =  'sellLimit.'.$transaction->user_id;
            $keys[] =  'buyLimit.'.$transaction->user_id;
        }


        $keys[] =  'getTransactionStatistic.'.$transaction->currency_id.$transaction->type_id;

        return $keys;
    }

    /**
     * @param Transaction $transaction
     * @return array
     * @throws \Exception
     */
    private function getCacheTags(Transaction $transaction): array
    {
        if (null == $transaction->user_id) {
            return [];
        }

        $tags = [
            //'userAllOperationsAll',
            //'userAllOperationsExchange',
            'totalByTransactionType',
            'userBalancesByCurrency.' . $transaction->user_id,
            'userAllOperations.' . $transaction->user_id,
            'userAccOperations.' . $transaction->user_id,
            'userAllOperationsExchange'. $transaction->user_id,
            'userTotalByTransactionType.' . $transaction->user_id.$transaction->type->name
        ];

        if ($transaction->type->name == 'enter') {
            $tags[] = 'userTotalDeposited.' . $transaction->user_id;
            $tags[] = 'totalDeposited';
        }


        if ($transaction->type->name == 'exchange_sell') {
            $tags[] = 'totalSell';
        }

        if ($transaction->type->name == 'charity') {
            $tags[] = 'userTotalCharity.' . $transaction->user_id;
        }

        if ($transaction->type->name == 'transfer_send') {
            $tags[] = 'userTotalSendTransfer.' . $transaction->user_id;
        }

        if ($transaction->type->name == 'transfer_receive') {
            $tags[] = 'userTotalReceivedTransfer.' . $transaction->user_id;
        }

        if ($transaction->type->name == 'bonus') {
            $tags[] = 'userTotalBonus.' . $transaction->user_id;
        }

        if ($transaction->type->name == 'partner') {
            $tags[] = 'userTotalPartner.' . $transaction->user_id;
        }

        if ($transaction->type->name == 'withdraw') {
            $tags[] = 'userTotalWithdrawn.' . $transaction->user_id;
            $tags[] = 'lastWithdrawals';
        }

        if ($transaction->type->name == 'dividend') {
            $tags[] = 'userTotalEarned.' . $transaction->user_id;
            $tags[] = 'lastEarnings';
        }

        return $tags;
    }

    /**
     * Listen to the Transaction created event.
     *
     * @param Transaction $transaction
     * @return void
     * @throws
     */
    public function created(Transaction $transaction)
    {
        clearCacheByArray($this->getCacheKeys($transaction));
        clearCacheByTags($this->getCacheTags($transaction));
    }

    /**
     * Listen to the Transaction deleting event.
     *
     * @param Transaction $transaction
     * @return void
     * @throws
     */
    public function deleted(Transaction $transaction)
    {
        clearCacheByArray($this->getCacheKeys($transaction));
        clearCacheByTags($this->getCacheTags($transaction));
    }

    /**
     * Listen to the Transaction updating event.
     *
     * @param Transaction $transaction
     * @return void
     * @throws
     */
    public function updated(Transaction $transaction)
    {
        clearCacheByArray($this->getCacheKeys($transaction));
        clearCacheByTags($this->getCacheTags($transaction));
    }
}