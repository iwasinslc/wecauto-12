<?php
namespace Database\Seeders;

use App\Models\UserRank;
use Illuminate\Database\Seeder;

/**
 * Class UserRanksSeeder
 */
class UserRanksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ranks = [
            [
                'name' => 'Client',
                'daily_limit' => 1.5,
                'levels'=>1,
                'confines' => json_encode([])
            ],
            [
                'name' => 'Manager',
                'daily_limit' => 3,
                'levels'=>2,
                'confines' => json_encode([
                    'licence' => 500,
                    'ranks' =>[
                        1 => 2
                    ]
                ])
            ],
            [
                'name' => 'Director',
                'daily_limit' => 4.5,
                'levels'=>3,
                'confines' => json_encode([
                    'licence' => 5000,
                    'ranks' =>[
                        2 => 2
                    ]
                ])
            ],
            [
                'name' => 'President',
                'daily_limit' => 7.5,
                'levels'=>5,
                'confines' => json_encode([
                    'licence' => 20000,
                    'ranks' =>[
                        3 => 2
                    ]
                ])
            ],
            [
                'name' => 'Shareholder',
                'daily_limit' => 15,
                'levels'=>10,
                'confines' => json_encode([
                    'licence' => 50000,
                    'ranks' =>[
                        4 => 2,
                    ]
                ])
            ],


        ];

        foreach ($ranks as $rank) {
            $searchType = UserRank::where('name', $rank['name'])->count();

            if ($searchType > 0) {
                echo "Rank '".$rank['name']."' already registered.\n";
                continue;
            }

            UserRank::create($rank);
            echo "Rank '".$rank['name']."' registered.\n";
        }
    }
}
