<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('type_id');
            $table->string('user_id');
            $table->string('currency_id');
            $table->string('rate_id')->nullable();
            $table->string('deposit_id')->nullable();
            $table->string('wallet_id');
            $table->string('payment_system_id')->nullable();
            $table->float('amount');
            $table->string('source')->nullable();
            $table->string('result')->nullable();
            $table->string('batch_id')->nullable();
            $table->float('commission')->nullable();
            $table->boolean('approved')->default(false);
            $table->mediumText('log')->nullable(); // what for?...
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
