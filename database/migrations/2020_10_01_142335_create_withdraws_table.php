<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraws', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('user_id');
            $table->string('currency_id');
            $table->string('wallet_id');
            $table->string('payment_system_id')->nullable();
            $table->float('amount');
            $table->string('source')->nullable();
            $table->string('result')->nullable();
            $table->string('batch_id')->nullable();
            $table->float('commission')->nullable();
            $table->unsignedTinyInteger('status_id')->default(0);
            $table->string('confirmation_code')->nullable();
            $table->dateTime('confirmation_sent_at')->nullable();
            $table->timestamps();

            $table->foreign('status_id')
                ->on('transaction_statuses')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraws');
    }
}
