@extends('layouts.profile')
@section('title', 'Мои заявки')

@section('content')
    <div class="cashback">
        <!---->
        <section>
            <div class="container">
                <ul class="tabs-navigation">
                    <li><a href="{{ route('profile.cashback.conditions') }}">Как получить Cash Back?</a>
                    </li>
                     @if(hasClosedDeposits()||user()->hasRole(['root']))
                    <li><a href="{{ route('profile.cashback.create') }}">Подать заявку</a>
                    </li>
                    <li class="is-active"><a href="{{ route('profile.cashback.index') }}">Мои заявки</a>
                    </li>
                     @endif
                </ul>
            </div>
        </section>
        <section class="claims">
            <div class="container">
                <h2 class="lk-title">Мои заявки
                </h2>
                <div class="content-block">
                    <div class="lk-table table-filter-hidden">
                        <table id="cashback-requests" class="js-datatables responsive nowrap no-sorting" data-page-length="30">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Дата подачи</th>
                                <th>Документы</th>
                                <th>Видеофайлы</th>
                                <th>Статус</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('load-scripts')
    <script>
        //initialize basic datatable
        var table = $('#cashback-requests').width('100%').DataTable({
            "processing": true,
            // "serverSide": true,
            "order": [[1, "desc"]],
            "ajax": "{{route('profile.cashback.dataTable')}}",
            "columns": [
                {"data": "DT_Row_Index", "orderable": false},
                {"data": "created_at"},
                {
                    "data": "documents_checked",
                    "render": function (data) {
                        return data
                            ? '<div class="status-icon status-icon--checked"></div>'
                            : '<div class="status-icon"></div>';
                    }
                },
                {
                    "data": "video_checked",
                    "render": function (data) {
                        return data
                            ? '<div class="status-icon status-icon--checked"></div>'
                            : '<div class="status-icon"></div>';
                    }
                },
                {"data": "status"},
                {
                    "data": "id",
                    "orderable": false,
                    "searchable": false,
                    "render": function (data) {
                        return '<a class="btn btn--warning" href="/profile/cashback/' + data + '">{{__('Подробнее о заявке')}}</a>'
                    }
                },

            ],
            @include('partials.lang_datatable')
        });
    </script>
@endpush