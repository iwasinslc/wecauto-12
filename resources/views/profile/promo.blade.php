@extends('layouts.profile')
@section('title', __('Promo'))
@section('content')
    <section>
        <div class="container">
            <h3 class="lk-title">Promo
            </h3>
            <div class="white-block">
                <div class="promo-block"><img src="/assets/images/banners/Gif-banner-01.gif" alt="">
                    <div class="promo-block__bottom"><a class="btn" download href="/assets/images/banners/Gif-banner-01.gif">Download</a>
                        <p class="promo-block__size">1920x260 px
                        </p>
                    </div>
                </div>
                <div class="promo-block"><img src="/assets/images/banners/Gif-banner-02.gif" alt="">
                    <div class="promo-block__bottom"><a class="btn" download href="/assets/images/banners/Gif-banner-02.gif">Download</a>
                        <p class="promo-block__size">1920x260 px
                        </p>
                    </div>
                </div>
                <div class="promo-block"><img src="/assets/images/banners/Gif-banner-03.gif" alt="">
                    <div class="promo-block__bottom"><a class="btn" download href="/assets/images/banners/Gif-banner-03.gif">Download</a>
                        <p class="promo-block__size">1920x260 px
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection