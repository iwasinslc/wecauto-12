@extends('layouts.profile')
@section('title', __('Exchange'))
@section('content')



    <section>
        <div class="container">
            @if ($mainCode=='FST')
                <div class="tabs-nav">
                    <div class="tabs-nav__row">

                        <ul>
                            <li class="{{$code=='WEC' ? 'is-active' :''}}"><a
                                        href="{{route('profile.exchange.currency', ['mainCode'=>'FST', 'code'=>'WEC'])}}">FST/WEC</a>
                            </li>
                            <li class="{{$code=='ACC' ? 'is-active' :''}}"><a
                                        href="{{route('profile.exchange.currency', ['mainCode'=>'FST','code'=>'ACC'])}}">FST/ACC
                                    <span style="color: green">({{__('Parking')}})</span>
                                </a></li>
                            {{--                        @if (user()->hasRole(['root']))--}}
                            {{--                            <li class="{{$code=='USD' ? 'is-active' :''}}"><a--}}
                            {{--                                        href="{{route('profile.exchange.currency', ['mainCode'=>'WEC', 'code'=>'USD'])}}">WEC/USD</a>--}}
                            {{--                            </li>--}}
                            {{--                        @endif--}}


                        </ul>
                    </div>
                </div>
            @endif

            <input type="hidden" id="limit_rate" value="{{rate($code, 'USD')}}">
            <div class="row">
                <div class="col">
                    <h4 class="lk-title">{!!__('Buy')!!} {{$mainCode}}
                    </h4>
                    <div class="buy-sell" id="buy">
                        <form class="buy-sell-offers" method="POST" action="{{ route('profile.exchange.exchange') }}">
                            <input type="hidden" name="type" value="{{\App\Models\ExchangeOrder::TYPE_BUY}}">
                            <input type="hidden" name="main_wallet_id" value="{{getUserWallet($mainCode)['id']}}">
                            {{ csrf_field() }}

                            <div class="buy-sell-offers__content">
                                <ul class="buy-sell-offers__list">
                                    <li><span>{!!__('You buy')!!}</span>
                                        <div class="field field--currency">
                                            <input type="number" class="amount" value="0" min="0" step="any"
                                                   name="amount"/><span class="field__currency">{{$mainCode}}</span>
                                        </div>
                                    </li>
                                    <li><span>{!!__('Price per')!!} {{$mainCode}}</span>
                                        <div class="field field--currency">
                                            <div class="field__group">
                                                <input class="rate" name="rate" min="0" step="any" value="0"
                                                       type="number"/>
                                                <label class="select">
                                                    <select name="wallet_id">
                                                        <option data-currency="{{$code}}"
                                                                value="{{getUserWallet($code)['id']}}">{{$code}}</option>
                                                    </select>
                                                </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span>{!!__('You will give')!!}</span>
                                        <div class="field field--currency">
                                            <input class="rate_amount" readonly type="number" min="0" value="0"><span
                                                    class="field__currency">{{$code}}</span>
                                        </div>
                                    </li>
                                </ul>
                                <div class="buy-sell-offers__bottom">
                                    <button class="btn btn--warning">{!!__('Buy')!!} {{$mainCode}}
                                    </button>
                                </div>
                                <div class="buy-sell-offers__bottom-info">
                                    <p class="color-warning">{!!__('You have')!!}: <b
                                                class="balance_{{getUserWallet($code)['id']}}">{{getUserWallet($code)['balance']}}</b> {{$code}}
                                    </p>
                                        @if ($mainCode=='FST')
                                    <p class="color-warning">{!!__('Daily Limit')!!}: <b>{{__('None')}}</b></p>
                                     @endif
                                </div>

                                @if ($mainCode=='FST')
                                    <div class="buy-sell-offers__bottom-info">
                                        <p class="color-warning">{!!__('Buy Limit')!!}: <b
                                                    id="buy_limit">{{user()->buyLimit()}}</b> USD</p>
                                        <p class="color-warning">{!!__('Removed Buy Limit')!!}: <b
                                                    class="removed_limit">0</b> USD</p>
                                    </div>
                                @else

                                    <div class="buy-sell-offers__bottom-info">
                                        <p class="color-primary">{!!__('Min price')!!}: <b>{{\App\Models\Setting::getValue('min_price_usd')}}</b> USD</p>
                                        <p class="color-primary">{!!__('Max Price')!!}: <b>{{\App\Models\Setting::getValue('max_price_usd')}}</b> USD</p>
                                    </div>
                                 @endif
                            </div>
                        </form>
                        <div class="orders orders--buy-sell">
                            <div class="orders__top">
                                <h4 class="lk-title">{!!__('Sale')!!} {{$mainCode}}
                                </h4>
                                <p class="orders__total">{!!__('Total')!!}: <strong
                                            id="total_sell">0 {{$mainCode}}</strong></p>
                            </div>
                            <div class="orders__content lk-table table-filter-hidden">
                                <table class="" data-page-length="999999" id="sell_orders">
                                    <thead>
                                    <tr>
                                        <th class="no-sorting">{!!__('Price')!!}</th>
                                        <th class="no-sorting">{!!__('Count')!!}</th>
                                        <th class="no-sorting">{!!__('Volume')!!}</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h4 class="lk-title">{!!__('Sell')!!} {{$mainCode}}
                    </h4>
                    <div class="buy-sell" id="sale">
                        <form class="buy-sell-offers" method="POST" action="{{ route('profile.exchange.exchange') }}">
                            <input type="hidden" name="type" value="{{\App\Models\ExchangeOrder::TYPE_SELL}}">
                            <input type="hidden" name="main_wallet_id" value="{{getUserWallet($mainCode)['id']}}">
                            {{ csrf_field() }}
                            <div class="buy-sell-offers__content">
                                <ul class="buy-sell-offers__list">
                                    <li><span>{!!__('You sell')!!}</span>
                                        <div class="field field--currency">
                                            <input type="number" class="amount" min="0" step="any" value="0"
                                                   name="amount"/><span class="field__currency">{{$mainCode}}</span>
                                        </div>
                                    </li>
                                    <li><span>{!!__('Price per')!!} {{$mainCode}}</span>
                                        <div class="field field--currency">
                                            <div class="field__group">
                                                <input class="rate" name="rate" min="0" step="any" value="0"
                                                       type="number"/>
                                                <label class="select">
                                                    <select name="wallet_id">
                                                        <option data-currency="{{$code}}"
                                                                value="{{getUserWallet($code)['id']}}">{{$code}}</option>
                                                    </select>
                                                </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span>{!!__('You will receive')!!}</span>
                                        <div class="field field--currency">
                                            <input class="rate_amount" readonly type="number" min="0" value="0"><span
                                                    class="field__currency">{{$code}}</span>
                                        </div>
                                    </li>
                                </ul>
                                <div class="buy-sell-offers__bottom">
                                    <button class="btn btn--primary">{!!__('Sell')!!} {{$mainCode}}
                                    </button>
                                </div>
                                <div class="buy-sell-offers__bottom-info">
                                    <p class="color-primary">{!!__('You have')!!}: <b
                                                class="balance_{{getUserWallet($mainCode)['id']}}">{{getUserWallet($mainCode)['balance']}}</b> {{$mainCode}}
                                    </p>


                                    @if ($mainCode=='FST')
                                        <p class="color-primary">{!!__('Daily Limit')!!}:
                                            <b>{{user()->dailyLimit()}}</b> USD</p>
                                    @endif

                                </div>

                                @if ($mainCode=='FST')
                                    <div class="buy-sell-offers__bottom-info">
                                        <p class="color-primary">{!!__('Sell Limit')!!}: <b
                                                    id="sell_limit">{{user()->sellLimit()}}</b> USD</p>
                                        <p class="color-primary">{!!__('Removed Sell Limit')!!}: <b
                                                    class="removed_limit">0</b> USD</p>
                                    </div>

                                @else

                                    <div class="buy-sell-offers__bottom-info">
                                        <p class="color-primary">{!!__('Min price')!!}: <b>{{\App\Models\Setting::getValue('min_price_usd')}}</b> USD</p>
                                        <p class="color-primary">{!!__('Max Price')!!}: <b>{{\App\Models\Setting::getValue('max_price_usd')}}</b> USD</p>
                                    </div>

                                @endif
                            </div>
                        </form>
                        <div class="orders orders--buy-sell">
                            <div class="orders__top">
                                <h4 class="lk-title">{!!__('Purchase')!!} {{$mainCode}}
                                </h4>
                                <p class="orders__total">{!!__('Total')!!}: <strong
                                            id="total_buy">0 {{$mainCode}}</strong></p>
                            </div>
                            <div class="orders__content lk-table table-filter-hidden">
                                <table class="" data-page-length="999999" id="buy_orders">
                                    <thead>
                                    <tr>
                                        <th class="no-sorting">{!!__('Price')!!}</th>
                                        <th class="no-sorting">{!!__('Count')!!}</th>
                                        <th class="no-sorting">{!!__('Volume')!!}</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="lk-table table-filter-hidden">
        <div class="container">
            <div class="tabs-nav">
                <div class="tabs-nav__row">
                    <h3 class="lk-title">{{__('My Orders list')}}</h3>
                </div>
            </div>
            <table id="orders-table" class="responsive nowrap">
                <thead>
                <tr>
                    <th>{{__('Date')}}</th>
                    <th>{{__('Type')}}</th>
                    <th>{{__('Pair')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Price')}}</th>
                    <th>{{__('Value')}}</th>
                    <th>{{__('Status')}}</th>
                </tr>
                </thead>

            </table>
        </div>
    </section>
    <section class="lk-table table-filter-hidden">
        <div class="container">
            <div class="tabs-nav">
                <div class="tabs-nav__row">
                    <h3 class="lk-title">{{ __('My trade history') }}</h3>
                </div>
            </div>
            <table id="operations-table" class="responsive nowrap">
                <thead>
                <tr>
                    <th>{{__('Date')}}</th>
                    <th>{{ __('Type') }}</th>
                    <th>{{ __('Amount') }}</th>
                    <th>{{ __('Price') }}</th>
                    <th>{{ __('Volume') }}</th>
                    <th>{{__('Fee')}}</th>
                </tr>
                </thead>

            </table>
        </div>
    </section>
    <section class="lk-table table-filter-hidden">
        <div class="container">
            <div class="tabs-nav">
                <div class="tabs-nav__row">
                    <h3 class="lk-title">{{ __('All trade history') }}</h3>
                </div>
            </div>
            <table id="all-operations-table" class="responsive nowrap">
                <thead>
                <tr>
                    <th>{{__('Date')}}</th>
                    <th>{{ __('Type') }}</th>
                    <th>{{ __('Amount') }}</th>
                    <th>{{ __('Price') }}</th>
                    <th>{{ __('Volume') }}</th>
                    <th>{{__('Fee')}}</th>
                </tr>
                </thead>

            </table>
        </div>
    </section>



    {{--    <div class="tabs-nav">--}}
    {{--        <div class="container">--}}
    {{--            <ul>--}}
    {{--                <li class="{{$code=='USD' ? 'is-active' :''}}"><a href="{{route('profile.exchange.currency', ['code'=>'USD'])}}">ACC/USD</a></li>--}}
    {{--                <li class="{{$code=='WEC' ? 'is-active' :''}}"><a href="{{route('profile.exchange.currency', ['code'=>'WEC'])}}">ACC/WEC--}}
    {{--                        <span style="color: green">({{__('Parking')}})</span>--}}
    {{--                    </a></li>--}}
    {{--                <li class="{{$code=='PZM' ? 'is-active' :''}}"><a href="{{route('profile.exchange.currency', ['code'=>'PZM'])}}">ACC/PZM</a></li>--}}
    {{--                <li class="{{$code=='BIP' ? 'is-active' :''}}"><a href="{{route('profile.exchange.currency', ['code'=>'BIP'])}}">ACC/BIP</a></li>--}}
    {{--                <li class="{{$code=='PZM' ? 'is-active' :''}}"><a href="{{route('profile.exchange.currency', ['code'=>'PZM'])}}">ACC/PZM</a></li>--}}
    {{--            </ul>--}}
    {{--        </div>--}}
    {{--    </div>--}}


    <!-- /.card -->
@endsection

@section('modal_content')




    <div class="modal" id="order-delete">
        <div class="modal-inner modal-inner--dark">
            <div class="modal-inner__close js-close-modal">
                <svg class="svg-icon">
                    <use href="/assets/icons/sprite.svg#icon-cross"></use>
                </svg>
            </div>
            <div class="modal-header modal-header--border-bottom">
                <h3 class="modal-title"><span>{{__('Are you sure?')}} </span>
                </h3>
            </div>
            <div class="modal-content">
                <div class="cont">
                    <p></p>

                </div>
            </div>
            <div class="modal-footer modal-footer--center">
                <form action="" method="POST">
                    {{csrf_field()}}
                    <button class="btn btn--green btn--size-lg"><span>{{__('Yes')}}</span></button>
                </form>
                <button type="button" class="btn btn--warning btn--size-lg js-close-modal"><span>{{__('No')}}</span>
                </button>
            </div>
        </div>
    </div>


@endsection

@push('load-scripts')
    {{--    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>--}}
    <script type="text/javascript">
                {{--var options = {--}}
                {{--    series: [{--}}
                {{--        data: {!! getRatesStatics() !!}--}}
                {{--    }],--}}
                {{--    chart: {--}}
                {{--        type: 'candlestick',--}}
                {{--        height: document.documentElement.clientWidth < 767 ?  220 : 390,--}}
                {{--        toolbar: {--}}
                {{--            autoSelected: 'pan',--}}
                {{--            show: true--}}
                {{--        },--}}
                {{--        zoom: {--}}
                {{--            enabled: true--}}
                {{--        }--}}
                {{--    },--}}
                {{--    plotOptions: {--}}
                {{--        candlestick: {--}}
                {{--            colors: {--}}
                {{--                upward: '#f97d00',--}}
                {{--                downward: '#002737'--}}
                {{--            }--}}
                {{--        }--}}
                {{--    },--}}
                {{--    xaxis: {--}}
                {{--        type: 'datetime',--}}
                {{--        // labels: {--}}
                {{--        //     show: true--}}
                {{--        // }--}}
                {{--    },--}}
                {{--    yaxis: {--}}
                {{--        tooltip: {--}}
                {{--            enabled: true--}}
                {{--        }--}}
                {{--    }--}}
                {{--};--}}

                {{--var chart = new ApexCharts(document.querySelector(".js-chart"), options);--}}
                {{--chart.render();--}}


        var buy_data = {};
        var sell_data = {};
    </script>
    <script>


        $('body').on('change, input, keyup', '.rate, .amount', function () {
            let amount = $(this).closest('form').find('.amount').val();

            var limit_rate = $('#limit_rate').val();

            let rate = $(this).closest('form').find('.rate').val();
            let rate_amount = $(this).closest('form').find('.rate_amount');
            let removed_limit = $(this).closest('form').find('.removed_limit');
            let type = $(this).closest('form').find('input[name=type]').val();


            let $amount = amount * rate * limit_rate;


            if (type == 0&&'{{$mainCode}}'=='FST') {


                let first = buy_data[Object.keys(buy_data)[0]];

                if (first.rate > rate) {

                    var $start_amount = 0;
                    var $sum = amount;
                    $.each(buy_data, function (key, value) {

                        if ($sum <= 0) return false;
                        if (value.sum >= $sum) {
                            $start_amount = $start_amount + $sum * value.rate * limit_rate;
                            $sum = $sum - value.sum;

                            return false;
                        }
                        $sum = $sum - $sum - value.sum;
                        $start_amount = $start_amount + value.sum * value.rate * limit_rate;
                    });

                    if ($sum > 0) {
                        $start_amount = $start_amount + $sum * rate * limit_rate;
                        console.log($start_amount);
                    }
                    $amount = $start_amount;
                }
            }


            rate_amount.val(rate * amount);
            removed_limit.html($amount);
        });

    </script>

    <script>

        $('body').on('click', '.close-order', function () {
            let id = $(this).data('id');
            let amount = $(this).data('amount');
            let currency = $(this).data('currency');

            let modal = $('#order-delete');

            modal.find('form').attr('action', '/profile/exchange/close-order/' + id);
            modal.find('.cont>p').html('#' + id + ' - ' + amount + ' ' + currency);
            window.Modal.openModal('order-delete');
        });


        my_orders = $('#orders-table').width('100%').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[0, "desc"]],
            "ajax": '{{route('profile.exchange.my_orders', ['mainCode'=>$mainCode,'code'=>$code])}}',
            "columns": [
                {
                    "data": "created_at"
                },
                {
                    "data": 'amount',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {

                        type = '{{__('Sell')}}';
                        if (row['type'] == 1) {
                            type = '{{__('Buy')}}';
                        }

                        return '<div class="status status--warning">' + type + '</div>';

                    }
                },

                {
                    "data": 'amount',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {


                        return row['main_currency']['code'] + '/' + row['currency']['code'];

                    }
                },

                {
                    "data": 'amount',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {


                        return row['new_amount'] + ' ' + row['main_currency']['code'];

                    }
                },


                {
                    "data": 'rate',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {


                        return row['rate'] + ' ' + row['currency']['code'];

                    }
                },

                {
                    "data": 'amount',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {


                        return row['value'] + ' ' + row['currency']['code'];

                    }
                },

                {
                    "data": 'amount',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {


                        if (row['active'] == 1 ) {
                            return '<button class="btn btn--silver-light btn--size-xs close-order" data-amount="' + row['amount'] + '" data-currency="' + row['main_currency']['code'] + '" data-id="' + row['id'] + '">{{__('Cancel')}}</button>';
                        }

                        if (row['active'] == 0) {
                            return '<div class="status status--gray">{{__('Closed')}}</div>'
                        }

                        return '{{__('Wait')}}';


                    }
                },


            ],

            @include('partials.lang_datatable')
        });
        //initialize basic datatable
        user_operations = $('#operations-table').width('100%').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[0, "desc"]],
            "ajax": '{{route('profile.exchange.dataTableExchange', ['mainCode'=>$mainCode,'code'=>$code])}}',
            "columns": [
                {
                    "data": "created_at", "render": function (data, type, row, meta) {
                        if (row['type'] == 0) {
                            return '<div class="time-indicator time-indicator--sale"><span>' + row['created_at'] + '</span></div>';
                        }
                        return '<div class="time-indicator time-indicator--buy"><span>' + row['created_at'] + '</span></div>';
                    }
                },
                {
                    "data": "type", "render": function (data, type, row, meta) {
                        if (row['type'] == 0) {
                            return '{{ __('Sell') }}';
                        }
                        return '{{ __('Buy') }}';
                    }
                },
                {
                    "data": 'amount',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        return row['amount'] + ' ' + row['main_currency']['code'];
                    }
                },
                {
                    "data": 'rate',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        return row['rate'] + ' ' + row['currency']['code'];
                    }
                },
                {
                    "data": 'rate_amount',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        return row['rate_amount'] + ' ' + row['currency']['code'];
                    }
                },
                {"data": "fee"},
            ],
            @if(app()->getLocale()=='ru')
            "language": {
                "url": "/json/Russian.json"
            },
            @endif
        });


        //*initialize basic datatable
    </script>
    <script>
        //initialize basic datatable
        all_operations = $('#all-operations-table').width('100%').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 100,
            "order": [0, "desc"],
            "ajax": '{{route('profile.exchange.dataTableExchangeAll', ['mainCode'=>$mainCode,'code'=>$code])}}',
            "columns": [
                // {
                //     "data": 'user.login',
                //     "orderable": true,
                //     "searchable": true,
                //     "render": function (data, type, row, meta) {
                //         return row['user']['login'];
                //     }
                // },
                {
                    "data": "created_at", "render": function (data, type, row, meta) {
                        if (row['type'] == 0) {
                            return '<div class="time-indicator time-indicator--sale"><span>' + row['created_at'] + '</span></div>';
                        }
                        return '<div class="time-indicator time-indicator--buy"><span>' + row['created_at'] + '</span></div>';
                    }
                },
                {
                    "data": "type", "render": function (data, type, row, meta) {
                        if (row['type'] == 0) {
                            return '{{ __('Sell') }}';
                        }
                        return '{{ __('Buy') }}';
                    }
                },
                {
                    "data": 'amount',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        return row['amount'] + ' ' + row['main_currency']['code'];
                    }
                },
                {
                    "data": 'order.rate',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        return row['rate'] + ' ' + row['currency']['code'];
                    }
                },
                {
                    "data": 'rate_amount',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        return row['rate_amount'] + ' ' + row['currency']['code'];
                    }
                },
                {"data": "fee"},
            ],
            @if(app()->getLocale()=='ru')
            "language": {
                "url": "/json/Russian.json"
            },
            @endif
        });


        //*initialize basic datatable
    </script>


    <script>
        //initialize basic datatable
        sell_orders = $('#sell_orders').DataTable({
            "language": {
                "decimal": ".",
                "thousands": ""
            },
            "processing": true,
            "serverSide": true,
            "columnDefs": [
                {type: 'num', targets: 0}
            ],
            "scrollY": "200px",
            "scrollCollapse": true,
            "paging": false,
            "order": [0, "asc"],
            "ajax": '{{route('profile.exchange.sell_orders', ['mainCode'=>$mainCode, 'code'=>$code])}}',
            "columns": [


                {
                    "data": "num_rate", "render": function (data, type, row, meta) {
                        return row['num_rate'];
                    }
                },


                {
                    "data": "sum", "render": function (data, type, row, meta) {
                        return '<strong class="color-warning">' + row['sum'] + ' {{$mainCode}}</strong>';
                    }
                },

                {
                    "data": "value", "render": function (data, type, row, meta) {
                        return row['rate'] * row['sum'] + ' {{$code}}';
                    }
                },

            ],
            fnInitComplete: function () {
                $(".dataTables_scrollBody").addClass("scrollbar-inner");
                $(".dataTables_scrollBody").scrollbar();
            },

            "footerCallback": function (row, data, start, end, display) {


                let obj = data[Object.keys(data)[0]];


                sell_data = data;

                if(obj) {
                    $('#buy .rate').val(obj.rate);

                    $('#total_sell').html(data.reduce((total, obj) => parseFloat(obj.sum) + parseFloat(total), 0).toFixed(4) + ' {{$mainCode}}');
                }
            },


            @include('partials.lang_datatable')
        });


        $('body').on('click', '#sell_orders tbody tr', function () {
            let row = sell_orders.row($(this)).data();
            $('#buy .rate').val(row.rate);
            $('#buy .amount').val(row.sum);
            $('#buy .rate_amount').val(row.rate * row.sum);

            var limit_rate = $('#limit_rate').val();
            let removed_limit = $('#buy').find('.removed_limit');
            let $amount = row.rate * row.sum * limit_rate;
            removed_limit.html($amount);
        });

        $('#sell_orders')
            .parents(".dataTables_wrapper")
            .find(".dataTables_scrollHead th")
            .each(function () {
                $(this).wrapInner("<span></span>");
            });

    </script>


    <script>

        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "formatted-num-pre": function (a) {
                a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                return parseFloat(a);
            },

            "formatted-num-asc": function (a, b) {
                return a - b;
            },

            "formatted-num-desc": function (a, b) {
                return b - a;
            }
        });
        //initialize basic datatable
        buy_orders = $('#buy_orders').DataTable({
            "language": {
                "decimal": ".",
                "thousands": ""
            },
            "processing": true,
            "serverSide": true,
            "scrollY": "400px",
            "scrollCollapse": true,
            "paging": false,
            "order": [0, "desc"],
            "columnDefs": [
                {type: 'num', targets: 0}
            ],
            "ajax": '{{route('profile.exchange.buy_orders', ['mainCode'=>$mainCode,'code'=>$code])}}',
            "columns": [


                {
                    "data": "num_rate", "render": function (data, type, row, meta) {
                        return row['num_rate'];
                    }
                },


                {
                    "data": "sum", "render": function (data, type, row, meta) {
                        return '<strong class="color-warning">' + row['sum'] + ' {{$mainCode}}</strong>';
                    }
                },

                {
                    "data": "value", "render": function (data, type, row, meta) {
                        return row['rate'] * row['sum'] + ' {{$code}}';
                    }
                },
            ],
            fnInitComplete: function () {
                $(".dataTables_scrollBody").addClass("scrollbar-inner");
                $(".dataTables_scrollBody").scrollbar();
            },

            "footerCallback": function (row, data, start, end, display) {
                let obj = data[Object.keys(data)[0]];


                buy_data = data;

                if(obj) {
                    $('#sale .rate').val(obj.rate);

                    $('#total_buy').html(data.reduce((total, obj) => parseFloat(obj.sum*obj.rate) + parseFloat(total), 0).toFixed(4) + ' {{$code}}');
                }
            },
            @include('partials.lang_datatable')
        });


        $('body').on('click', '#buy_orders tbody tr', function () {
            let row = buy_orders.row($(this)).data();
            $('#sale .rate').val(row.rate);
            $('#sale .amount').val(row.sum);
            $('#sale .rate_amount').val(row.rate * row.sum);
            var limit_rate = $('#limit_rate').val();
            let removed_limit = $('#sale').find('.removed_limit');
            let $amount = row.rate * row.sum * limit_rate;
            let first = buy_data[Object.keys(buy_data)[0]];
            if (first.rate > row.rate) {

                var $start_amount = 0;
                var $sum = row.sum;
                $.each(buy_data, function (key, value) {

                    if ($sum <= 0) return false;
                    if (value.sum >= $sum) {
                        $start_amount = $start_amount + $sum * value.rate * limit_rate;
                        $sum = $sum - value.sum;

                        return false;
                    }
                    $sum = $sum - $sum - value.sum;
                    $start_amount = $start_amount + value.sum * value.rate * limit_rate;
                });

                if ($sum > 0) {
                    $start_amount = $start_amount + $sum * row.rate * limit_rate;
                }
                $amount = $start_amount;
            }


            console.log($amount);

            removed_limit.html($amount);
        });

        $('#buy_orders')
            .parents(".dataTables_wrapper")
            .find(".dataTables_scrollHead th")
            .each(function () {
                $(this).wrapInner("<span></span>");
            });


        window.Echo.private('exchange.user.{{user()->id}}').listen("ExchangeUserOperations", (e) => {
            console.log('ExchangeUserOperations');
            let sell = $('#sell_limit');
            if (sell.length) {

                sell.html((e.data.sell_limit).toLocaleString('en-En'))
            }

            let buy = $('#buy_limit');
            if (buy.length) {

                buy.html((e.data.buy_limit).toLocaleString('en-En'))
            }
            // my_orders.ajax.reload();
            // user_operations.ajax.reload();
        });


        //*initialize basic datatable
    </script>



@endpush



