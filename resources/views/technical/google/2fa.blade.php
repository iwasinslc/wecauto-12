@extends('layouts.auth')
@section('title', __('Google Authenticator One Time Password'))

@section('content')

    <div class="auth__content">
        <div class="auth-module auth-module--not-content">
            <form class="auth-module__form" method="POST" action="{{ route('profile.settings') }}">
                {{ csrf_field() }}
                <h3 class="auth-module__title">{{__('Google Authenticator One Time Password')}}
                </h3>
                <!-- .field--error-->
                <div class="field field--row">
                    <label>{{ __('Enter one time password') }}</label>
                    <input class="field-stroke" type="text" name="one_time_password" required>
                </div>

                <div class="auth-module__buttons">
                    <button class="btn btn--warning btn--size-lg">{{ __('Pass 2FA') }}
                    </button>

                </div>
            </form>
        </div>
    </div>


@endsection