<section class="info-block">
    <div class="container">
        <p class="main-info-message color-warning" style="margin-bottom: 20px;">
            {{__('Confirm your email address for withdrawals, transfers or exchanges. If the letter did not arrive, check the "Spam" section')}}

        </p>
        <div class="main-info-message-btns">
            <form action="{{route('profile.send_verification_email')}}">
                <button class="btn btn--warning btn--size-sm">{{__('Send verification email')}}
                </button>
            </form>
        </div>
    </div>
</section>
