<aside class="lk__sidebar">
    <div class="js-sticky-sidebar">
        <div class="navigation-icons">
            <ul>



                <li class="{{ (Route::is('profile.profile') ? 'active' : '') }}"><a href="{{route('profile.profile')}}"><span class="navigation-icons__icon">
                    <svg class="svg-icon">
                      <use href="/assets/icons/sprite.svg#icon-account"></use>
                    </svg></span><span>{{__('Account')}}</span></a>
                </li>
                <li class="{{ (Route::is('profile.transfer')||Route::is('profile.topup')||Route::is('profile.withdraw')||Route::is('profile.acc_withdraw') ? 'active' : '') }}"><a href="{{route('profile.topup')}}"><span class="navigation-icons__icon">
                    <svg class="svg-icon">
                      <use href="/assets/icons/sprite.svg#icon-003-speed"></use>
                    </svg></span><span>{{__('Finance')}}</span></a>
                </li>


                <li class="{{ (Route::is('profile.affiliate') ? 'active' : '') }}"><a href="{{route('profile.affiliate')}}"><span class="navigation-icons__icon">
                    <svg class="svg-icon">
                      <use href="/assets/icons/sprite.svg#icon-007-deal"></use>
                    </svg></span><span>{{__('Referral')}}</span></a>
                </li>


                <li class="{{ (Route::is('profile.settings') ? 'active' : '') }}"><a href="{{route('profile.settings')}}"><span class="navigation-icons__icon">
                    <svg class="svg-icon">
                      <use href="/assets/icons/sprite.svg#icon-001-settings"></use>
                    </svg></span><span>{{__('Settings')}}</span></a>
                </li>


                    <li class="{{ (Route::is('profile.exchange') || Route::is('profile.exchange.*') ? 'active' : '') }}"><a href="{{route('profile.exchange')}}"><span class="navigation-icons__icon">
                    <svg class="svg-icon">
                      <use href="/assets/icons/sprite.svg#icon-003-speed"></use>
                    </svg></span><span>{{__('Buy')}} FST</span></a>
                    </li>


                    <li class="{{ (Route::is('profile.exchange_wec') ? 'active' : '') }}"><a href="{{route('profile.exchange_wec')}}"><span class="navigation-icons__icon">
                    <svg class="svg-icon">
                      <use href="/assets/icons/sprite.svg#icon-003-speed"></use>
                    </svg></span><span>{{__('Buy')}} WEC</span></a>
                    </li>





                    <li class="{{ (Route::is('profile.licence') ? 'active' : '') }}"><a href="{{route('profile.licence')}}"><span class="navigation-icons__icon">
                    <svg class="svg-icon">
                      <use href="/assets/icons/sprite.svg#icon-004-certificate"></use>
                    </svg></span><span>{{__('Buy License')}}</span></a>
                    </li>

                    <li class="{{ (Route::is('profile.deposits.*') ? 'active' : '') }}"><a href="{{route('profile.deposits')}}"><span class="navigation-icons__icon">
                    <svg class="svg-icon">
                      <use href="/assets/icons/sprite.svg#icon-006-car"></use>
                    </svg></span><span>{{__('Car order')}}</span></a>
                    </li>
{{--                @hasrole('root|admin')--}}

                    <li class="{{ (Route::is('profile.cashback.*') ? 'active' : '') }}">
                        <a href="{{ route('profile.cashback.conditions') }}">
                            <span class="navigation-icons__icon">
                            <svg class="svg-icon">
                              <use href="/assets/icons/sprite.svg#icon-cashback"></use>
                            </svg>
                            </span>
                            <span>{{__('Cashback')}}</span>
                        </a>
                    </li>

                    @hasrole('root|moderator')
                        <li class="{{ (Route::is('profile.cashback-result.*') ? 'active' : '') }}">
                            <a href="{{ route('profile.cashback-result.index') }}">
                                    <span class="navigation-icons__icon">
                                    <svg class="svg-icon">
                                      <use href="/assets/icons/sprite.svg#icon-cashback"></use>
                                    </svg>
                                    </span>
                                <span>{{__('Cashback results')}}</span>
                            </a>
                        </li>

                    @endhasrole
{{--                @endhasrole--}}

                    <li class="{{ (Route::is('profile.promo') ? 'active' : '') }}"><a href="{{route('profile.promo')}}"><span class="navigation-icons__icon">
                    <svg class="svg-icon">
                      <use href="/assets/icons/sprite.svg#icon-008-microphone"></use>
                    </svg></span><span>{{__('Promo')}}</span></a>
                    </li>
                    <li class="{{ (Route::is('profile.charity') ? 'active' : '') }}"><a href="{{route('profile.charity')}}"><span class="navigation-icons__icon">
                    <svg class="svg-icon">
                      <use href="/assets/icons/sprite.svg#icon-heart"></use>
                    </svg></span><span>{{__('Charity')}}</span></a>
                    </li>







            </ul>
        </div>
    </div>
</aside>