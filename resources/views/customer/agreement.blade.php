@extends('layouts.auth')
@section('title', __('Agreement'))
@section('content')
    <section class="page-preview">
        <div class="container">
                                <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/</span><span>{{__('Terms and Conditions')}}</span>
            </div>
          
<h1 class="page-title">{{__('Wecauto Cooperation Terms and Conditions')}}
                    </h1>
                </div>
                    </section>
    <section class="terms">
        <div class="container">
            <div class="typography">
                <p class="typography-intro-text"> <strong>
                    <p>{!! __('These Terms and Conditions regulate the relationship between Webtokenprofit (hereinafter referred to as the Company) and the website user (hereinafter referred to as the Customer) and are subject to mandatory execution by both parties throughout the entire cooperation. The Terms and Conditions duration begins since the moment of their publication in this section and ends at the moment of cooperation termination with Webtokenprofit.') !!}</p>
                    <h4>{!! __('1.	General Terms and Conditions') !!}</h4>
                    <p>{!! __("1.1.	The Company's Customer may be a physical person who is of full age and agrees to unconditionally comply with these Terms and Conditions. Registration on the platform implies the Customer's full consent to all Terms and Conditions.") !!}</p>
                    <p>{!! __('1.2.	The refusal to comply with the Terms and Conditions or violation thereof is cause to terminate cooperation.') !!}</p>
                    <p>{!! __('1.3.	The Company has the right to make changes in the marketing plan without making mandatory written notice to users.') !!}</p>
                    <h4>{!! __('2.	Corporate guarantees and liabilities') !!}</h4>
                    <p>{!! __('2.1.	The Company guarantees to provide the Customer with access to all Platform services immediately after successful completion of the registration procedure.') !!} </p>
                    <p>{!! __("2.2.	The Company guarantees profit accruing to the Customer's account within the scope in the corporate marketing plan and in the terms of the license purchased by the Customer.") !!}</p>
                    <p>{!! __('2.3.	The Company agrees and undertakes to make payments within the terms that were previously agreed in the Cooperation Terms and Conditions.') !!}</p>
                    <p>{!! __('2.4.	The Company guarantees accruing of affiliate rewards within the scope in the referral program regardless of the Customer’s financial activity on the website in accordance with the terms of the current marketing plan.') !!}</p>
                    <p>{!! __("2.5.	The Company agrees and undertakes to ensure website successful operation and the prompt elimination of any operating failures that affect in any manner the Customer's activity on the website.") !!}</p>
                    <p>{!! __("2.6.	The Company guarantees confidentiality and protects any Customer's data from third parties under the terms of the Privacy Policy.") !!}</p>
                    <p>{!! __('2.7.	The Company is not responsible for the operating failures or other challenges associated with the incorrect operation of electronic payment systems used by the Customer in financial transactions.') !!}</p>
                    <p>{!! __('2.8.	The Company guarantees the maintenance of user feedback by the Support Service or direct e-mail.') !!}</p>
                    <h4>{!! __("3.	Customer's responsibility and rights") !!}</h4>
                    <p>{!! __('3.1.	The Customer has the right to use all opportunities provided by the Company to make profit.') !!}</p>
                    <p>{!! __('3.2.	The Customer agrees and undertakes to provide only authentic data when registring.') !!}</p>
                    <p>{!! __('3.3.	The Customer has the right to participate in the referral program and receive rewards under the terms and conditions of the referral program.') !!}</p>
                    <p>{!! __('3.4.	Unauthorized or erroneous transactions made by the Customer have no retroactive force. The Company does not compensate possible monetary losses incurred by the Customer through his/her fault. The Customer is responsible for specifying correct payment details when withdrawing funds from the system.') !!}</p>
                    <p>{!! __('3.5.	Software, technologies used on the website and copyrighted materials are protected by copyright and may not be copied or reproduced in part or in full on any other resource without prior written permission of the Company. The Company has copyright and intellectual rights to information materials, software and website design. Any unauthorized copying of content is prohibited and may be prosecuted under law.') !!}</p>
                    <p>{!! __("3.6.	The Customer agrees and undertakes to preserve and maintain the Company's reputation and not to cause the Company any intentional reputational or any other damage. When the Customer performs actions that threaten corporate security, image and financial standing, his/her account will be blocked along with the financial resources on his/her balance.") !!}</p>
                    <p>{!! __('3.7.	The Customer has the right to distribute truthful information on the Company in order to attract partners under the terms of the affiliate program. At the same time, the Customer is not allowed to create new accounts using his/her own affiliate link, and also log in to two or more accounts using one device. Such violation may lead to the blocking of all detected clone accounts along with the financial resources on their balances.') !!}</p>
                    <p>{!! __('3.8.	The Customer is forbidden to use in his work with the Platform financial resources obtained illegally and fraudulently, bypassing the international legislation.') !!}</p>
                    <p>{!! __('3.9.	When using this platform, the Customer is responsible for tax compliance in his/her country of residence. The Customer agrees and undertakes to independently resolve issues related to tax rates.') !!}</p>
                     <h4>{!! __('4.	Changes in the Terms and Conditions') !!}</h4>
                    <p>{!! __('4.1.	The Company has the right to make amendments, changes and additions to the Terms and conditions followed by posting of the updated version on the website. The new version becomes effective on the date of publication. Further usage of the website automatically means the Customer agrees the updated Terms and Conditions.') !!}</p>
                    <h4>{!! __('5.	Cooperation Termination Methods') !!}</h4>
                    <p>{!! __('5.1.	Either party has the right to refuse to continue cooperation.') !!}</p>
                    <p>{!! __('5.2.	The Company has the right to terminate cooperation with the Customer who violates these Terms and Conditions or is involved in fraudulent activities.') !!}</p>
                    <p>{!! __("5.3.	The Customer has the right to refuse cooperation if the Company's activity contradicts the laws of his/her country of residence or if he/she disagrees with any provisions hereof.") !!}</p>
                    <p>{!! __('5.4.	The Customer independently determines cooperation terms with the Company and may terminate it at any time without prior notification and explanation of reasons.') !!}</p>
                    <h4>{!! __('6.	Disputable situations resolution') !!}</h4>
                    <p>{!! __('6.1.	If any disputes arise in the course of between the Company’s and the Customer’s cooperation, the search for a compromise decision is carried out by negotiations between both parties. If it is impossible to make such decision, the situation can be transferred to the legal level.') !!}</p>
                    <p>{!! __('6.2.	If any difficulties with account replenishment or withdrawal of funds, the Customer can contact the Support Service to solve the problem.') !!}</p>

                 </div>
            </div>
        </section>
    </article>
@endsection