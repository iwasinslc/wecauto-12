@extends('layouts.customer')
@section('title', __('Support'))
@section('content')
    <article class="page">
        <section class="contacts">
            <div class="container">
                <div class="page-top">
                    <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/</span><span>{{__('Contacts')}}</span>
                    </div>
                    <h1 class="page-title">{{__('Contacts')}}
                    </h1>
                </div>
                <div class="contacts__row">
                    <div class="contacts__col">
                        <ul class="contacts-list">
{{--                            <li>--}}
{{--                                <div class="contacts-item">--}}
{{--                                    <div class="contacts-item__icon">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-map"></use>--}}
{{--                                        </svg>--}}
{{--                                    </div>--}}
{{--                                    <div class="contacts-item__content">--}}
{{--                                        <p class="contacts-item__title">{{__('Address')}}:--}}
{{--                                        </p>--}}
{{--                                        <div class="contacts-item__desc">--}}
{{--                                            <p>38 Kinsland Road, London, UK, E28DD</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <div class="contacts-item">--}}
{{--                                    <div class="contacts-item__icon">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-phone"></use>--}}
{{--                                        </svg>--}}
{{--                                    </div>--}}
{{--                                    <div class="contacts-item__content">--}}
{{--                                        <p class="contacts-item__title">{{__('Phone')}}:--}}
{{--                                        </p>--}}
{{--                                        <div class="contacts-item__desc">--}}
{{--                                            <p>+441748220068</p>--}}
{{--                                            <p>(9:00 AM - 2:00 PM GMT) mon-fri</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                            <li>
                                <div class="contacts-item">
                                    <div class="contacts-item__icon">
                                        <svg class="svg-icon">
                                            <use href="/assets/icons/sprite.svg#icon-telegram"></use>
                                        </svg>
                                    </div>
                                    <div class="contacts-item__content">
                                        <p class="contacts-item__title">Telegram {{__('Chat')}}:
                                        </p>
                                        <div class="contacts-item__desc">
                                            <p><a href="https://t.me/crypto_accelerator">https://t.me/crypto_accelerator</a></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="contacts-item">
                                    <div class="contacts-item__icon">
                                        <svg class="svg-icon">
                                            <use href="/assets/icons/sprite.svg#icon-support"></use>
                                        </svg>
                                    </div>
                                    <div class="contacts-item__content">
                                        <p class="contacts-item__title">{{__('Support')}} Telegram:
                                        </p>
                                        <div class="contacts-item__desc">
                                            <p><a href="https://t.me/WtpSupportBot">@WtpSupportBot</a></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
{{--                            <li>--}}
{{--                                <div class="contacts-item">--}}
{{--                                    <div class="contacts-item__icon">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-fax"></use>--}}
{{--                                        </svg>--}}
{{--                                    </div>--}}
{{--                                    <div class="contacts-item__content">--}}
{{--                                        <p class="contacts-item__title">Fax:--}}
{{--                                        </p>--}}
{{--                                        <div class="contacts-item__desc">--}}
{{--                                            <p>+44 2070-433-811</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <div class="contacts-item">--}}
{{--                                    <div class="contacts-item__icon">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-mail"></use>--}}
{{--                                        </svg>--}}
{{--                                    </div>--}}
{{--                                    <div class="contacts-item__content">--}}
{{--                                        <p class="contacts-item__title">{{__('Email')}}:--}}
{{--                                        </p>--}}
{{--                                        <div class="contacts-item__desc">--}}
{{--                                            <p>support@crypto-accelerator.io</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                        </ul>
                    </div>
{{--                    <div class="contacts__col">--}}
{{--                        <div class="form-block">--}}
{{--                            <h4 class="form-block__title-mark">{{__('if you have any questions please do not hesitate to contact us')}}--}}
{{--                            </h4>--}}

{{--                            <form method="POST" target="_top" class="contacts-form" action="{{ route('customer.support') }}">--}}
{{--                                    {{ csrf_field() }}--}}
{{--                                <div class="contacts-form__desc">--}}
{{--                                    <p>{{__('We have developed a range of investment plans so that you can choose the one that fits the best with your investment strategy.')}}</p>--}}
{{--                                </div>--}}
{{--                                <div class="field">--}}
{{--                                    <label>{{__('Your name')}}:</label>--}}
{{--                                    <input type="text" class="{{$errors->has('name') ? 'error' : ''}}" name="name">--}}
{{--                                </div>--}}
{{--                                <div class="field">--}}
{{--                                    <label>{{ __('Email') }}:</label>--}}
{{--                                    <input name="email" class="{{$errors->has('email') ? 'error' : ''}}" type="text">--}}
{{--                                </div>--}}
{{--                                <div class="field">--}}
{{--                                    <label>{{ __('Question') }}:</label>--}}
{{--                                    <input type="text"  name="text">--}}
{{--                                </div>--}}
{{--                                <div class="contacts-form__bottom">--}}
{{--                                    <button disabled="disabled" class="btn btn--warning btn--size-md">{{ __('Send message') }}--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                        <div class="follow follow--large">--}}
{{--                            <ul>--}}
{{--                                <li><a href="#" target="_blank">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-vk"></use>--}}
{{--                                        </svg></a>--}}
{{--                                </li>--}}
{{--                                <li><a href="#" target="_blank">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-twitter"></use>--}}
{{--                                        </svg></a>--}}
{{--                                </li>--}}
{{--                                <li><a href="#" target="_blank">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-instagram"></use>--}}
{{--                                        </svg></a>--}}
{{--                                </li>--}}
{{--                                <li><a href="#" target="_blank">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-facebook"></use>--}}
{{--                                        </svg></a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </section>
    </article>
@endsection