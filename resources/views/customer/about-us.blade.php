@extends('layouts.auth')
@section('title', __('Marketing'))
@section('content')
    <section class="page-preview">
        <div class="container">
            <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/ </span><span>{{__('Marketing')}}</span>
            </div>
            <h1 class="page-preview__title">{{__('Marketing')}}
            </h1>
        </div>
    </section>
    <section class="about-info">
        <div class="container">
            <div class="typography">
                <blockquote>
                    <p>Участие в проекте WECAUTO допускается при покупке клиентом лицензии и внесении первоначального взноса в размере 30% от стоимости выбранного автомобиля или мотоцикла. Автодепозит действует на протяжении 12 месяцев со дня внесения первого взноса. После истечения данного периода клиент получает всю сумму на приобретение авто- мототранспорта.</p>
                </blockquote>
                <h4 class="color-warning text-uppercase">Покупка лицензии</h4>
                <p>Покупка лицензии позволяет клиенту стать полноправным участником системы автодепозитов. На выбор предлагается 5 лицензий с разным сроком действия, разными лимитами сумм на покупку автомобилей, а также с разными лимитами на покупку и продажу фастеров (FST), ускоряющих процесс полной покупки автомобиля либо мотоцикла.</p>
            </div>
            <div class="licenses">
                <div class="js-swiper-licenses swiper-container swiper-no-swiping">
                    <div class="swiper-wrapper">
                        @foreach($licences as $licence)
                            <div class="swiper-slide">
                                <div class="license-item">
                                    <p class="license-item__name">{{__($licence->name)}} {{$licence->id}}
                                    </p>
                                    <div class="license-item__price">
                                        <div class="license-item__price-count"><span><span class="color-warning">$</span>{{$licence->price}}</span>
                                        </div>
                                    </div>

                                    <p class="license-item__subtitle">{!! __('Duration') !!} — <span class="color-warning">{{$licence->duration}} {!! __('days') !!}</span>
                                    </p>

                                    <ul class="license-item__list">
                                        <li>{!! __('Price') !!} -  <strong> {{$licence->price*rate('USD', $licence->currency->code)}} {{$licence->currency->code}}  </strong>
                                        </li>
                                        <li>{!! __('Minimum car price') !!} - <strong>${{$licence->deposit_min}}</strong></li>
                                        <li>{!! __('Minimum motorcycle price') !!} - <strong>${{$licence->moto_min}}</strong></li>
                                        <li>{!! __('The most affordable price for a car or motorcycle') !!} - <strong>${{$licence->deposit_max}}</strong></li>
                                        <li>{!! __('Maximum purchase amount FST') !!} <strong>${{$licence->buy_amount}}</strong>
                                        </li>
                                        <li>{!! __('Maximum FST Sale Amount') !!} <strong>${{$licence->sell_amount}}</strong>
                                        </li>
                                    </ul>
                                    {{--                            <div class="license-item__desc">--}}
                                    {{--                                <p>Car price $7000</p>--}}
                                    {{--                                <p><small>(max)</small></p>--}}
                                    {{--                            </div>--}}
                                    <div class="license-item__buttons">
                                            <a href="{{route('login')}}" class="btn js-modal">{{__('Activate')}}
                                            </a>


                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <div class="slider-dots">  </div>
                </div>
                <div class="licenses__note">
                    <p>Каждая лицензия позволяет сделать заказ на 1 авто или 1 мотоцикл.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="fasters">
        <div class="composition-fasters" data-emergence="hidden"><img class="composition-fasters__cubes" src="/assets/images/composition-fasters.png" alt="" role="presentation"/><img class="composition-fasters__car" src="/assets/images/fasters-car.png" alt="" role="presentation"/><img class="composition-fasters__fst" src="/assets/images/fst2.png" alt="" role="presentation"/>
        </div>
        <div class="container">
            <div class="fasters__row">
                <div class="fasters__col">
                    <div class="typography">
                        <p> <strong>
                                <big>Срок вклада составляет 12 месяцев, однако, используя фастеры FST, его можно сокращать вплоть до 3 месяцев.</big></strong></p>
                        <p>Чтобы сократить депозитный период максимально, необходимо приобрести фастеров на общую сумму в расчёте 20% от заявленной стоимости машины в долларовом эквиваленте. Фастер используется исключительно для приближения срока получения средств на покупку автомобиля. FST можно приобретать и активировать частями, при этом каждая доля сокращает дни пропорционально своей стоимости (вся информация отображается в личном кабинете и всегда актуальна).</p>
                    </div>
                </div>
                <div class="fasters__col">
                    <h4 class="title-arrow"> <span>Лимиты дневных продаж фастеров FST</span>
                    </h4>
                    <div class="info-block">
                        <h5 class="info-block__title"> <span>Реализация фастеров FST осуществляется по принципу р2р, согласно ежедневным лимитам, установленным для каждого ранга в пределах общего лимита активной лицензии, на внутренней бирже сайта автопрограммы WECAUTO</span>
                        </h5>
                        <div class="typography">
                            <p> <i>Информацию о лимитах дневных продаж смотрите в разделе «Партнерская программа и ранги»</i></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="gray-block">
        <div class="container">
            <div class="gray-block__block">
                <div class="gray-block__row">
                    <div class="gray-block__col">
                        <div class="rhombus-item">
                            <div class="rhombus-item__icon">
                                <svg class="svg-icon">
                                    <use href="/assets/icons/sprite.svg#icon-website"></use>
                                </svg>
                            </div>
                            <p class="rhombus-item__content">Алгоритмы роста фастеров
                            </p>
                        </div>
                        <div class="typography">
                            <p>Стоимость фастеров напрямую зависит от общего объёма взносов всех участников в автопрограмму. При достижении каждого миллиона долларов стоимость FST увеличивается в 2 раза по отношению к предыдущему значению.</p>
                            <p>Например, после того, как общий объём автодепозитов достигнет $1 000 000, цена FST будет умножена на 2. Соответственно, при достижении общей суммы взносов $3 000 000 стоимость FST будет в 8 раз выше стартовой и т.д.</p>
                        </div>
                    </div>
                    <div class="gray-block__col">
                        <div class="rhombus-item">
                            <div class="rhombus-item__icon">
                                <svg class="svg-icon">
                                    <use href="/assets/icons/sprite.svg#icon-shuttle"></use>
                                </svg>
                            </div>
                            <p class="rhombus-item__content">Как получать фастеры?
                            </p>
                        </div>
                        <div class="typography">
                            <p> <strong>Есть 5 способов получения фастеров, а именно:</strong></p>
                            <ol class="circle-number-list-small">
                                <li>покупка лицензии (кэшбек в FST в размере 3% от суммы покупки); </li>
                                <li>участие в партнёрской программе и приглашение других участников в проект; </li>
                                <li>покупка FST у других участников на бирже P2P; </li>
                                <li>Паркинг ACC | FST - ежедневная добыча FST. При открытии ордера ACC/FST происходит ежедневная добыча FST из расчета за 1АСС - 0,5$ в FST по курсу сайта на момент начисления. Начисления за паркинг производятся ежедневно после 12.00 по МСК до момента срабатывания ордера.; </li>
                                <li>предоставление документов и видеоотчёта о покупке автомобиля через нашу систему.</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="partner-program">
        <div class="container">
            <h3 class="section-title"><span class="color-warning">Партнёрская программа</span> и ранги
            </h3>
            <p class="partner-program__subtitle">В проекте WECAUTO действует 10-уровная реферальная система
            </p>
            <div class="refferal-levels">
                <ul>
                    <li>
                        <div class="refferal-level">
                            <big>1<sup>st</sup> </big><span>level</span>
                        </div>
                        <div class="refferal-percent">10<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>2<sup>nd</sup> </big><span>level</span>
                        </div>
                        <div class="refferal-percent">5<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>3<sup>rd</sup> </big><span>level</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>4<sup>th</sup> </big><span>level</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>5<sup>th</sup> </big><span>level</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>6<sup>th</sup> </big><span>level</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>7<sup>rd</sup> </big><span>level</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>8<sup>th</sup> </big><span>level</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>9<sup>th</sup> </big><span>level</span>
                        </div>
                        <div class="refferal-percent">5<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>10<sup>th</sup> </big><span>level</span>
                        </div>
                        <div class="refferal-percent">10<sup>%</sup>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="partner-program__row">
                <div class="partner-program__col">
                    <div class="typography">
                        <p> <strong>
                                <big>на 1 уровне реферальный бонус составляет 10% от стоимости лицензии приглашённого реферала; на 2 —5%; на 3 — 3%; 4-й — 3%; 5-й — 3%; 6-й — 3%; 7-й — 3%; 8-й — 3%; 9-й — 5%; 10-й — 10%.</big></strong></p>
                        <p>Партнёрские вознаграждения начисляются в FST. В партнёрской программе предусмотрено 5 рангов:</p>
                        <ul class="before-warning">
                            <li> <strong>Партнёр; </strong></li>
                            <li> <strong>Менеджер; </strong></li>
                            <li> <strong>Директор; </strong></li>
                            <li> <strong>Президент; </strong></li>
                            <li> <strong>Акционер.</strong></li>
                        </ul>
                        <p>Статус «Партнер» присваивается любому участнику, активировавшему лицензию любой стоимости.</p>
                        <p>Чтобы повысить статус «Партнер» до статуса «Менеджер», необходимо приобрести лицензию стоимостью не менее $500, а также иметь в своей сети не менее 3-х лично приглашенных участников, активировавших любую лицензию и внёсших депозит на покупку авто или мото. </p>
                        <p>Для ранга «Менеджер» доступны 2 уровня партнёрской программы. Для ранга «Директор» нужно иметь в своей первой линии как минимум 2-х участников в ранге «Менеджер», а также активную лицензию стоимостью не менее $5000.</p>
                        <p>Ранг «Президент» присваивается участнику, имеющему в своей первой линии как минимум 2-х участников в ранге «Директор», а также активную лицензию стоимостью как минимум $20000. </p>
                        <p>В ранге «Президент» доступны 4 и 5 уровни партнёрской программы. Самый высокий ранг — «Акционер» — доступен при наличии у клиента лицензии самого высокого уровня и не менее 2 участников в ранге  «Президент» в первой линии.
                            Участнику в ранге «Акционер» доступны все уровни реферальной системы. </p>
                        <p>При покупке последующей лицензии ниже стоимости лицензии, соответствующей присвоенному рангу, реферальные начисления производятся согласно условиям ранга вновь приобретенной  лицензии.</p>
                    </div>
                </div>
                <div class="partner-program__col">
                    <div class="partner-program__image"><img src="/assets/images/become.png" alt="Become our partner">
                    </div>
                    <div class="info-block">
                        <h5 class="info-block__title"> <span>Дневные лимиты продаж фастеров FST для каждого из рангов: </span>
                        </h5>
                        <div class="typography">
                            <p>Ранг Партнер: дневной лимит на продажу FST - 1,5 % </p>
                            <p>Ранг Менеджер: дневной лимит на продажу FST - 3,0 % </p>
                            <p>Ранг Директор: дневной лимит на продажу FST - 4,5 %</p>
                            <p>Ранг Президент: дневной лимит на продажу FST - 7,5 % </p>
                            <p>Ранг Акционер: дневной лимит на продажу FST - 15,0 %</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection